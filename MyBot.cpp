#include "hlt/hlt.hpp"
#include "hlt/navigation.hpp"
#include "hlt/globals.h"
#include "src/base_functions.h"
#include "src/logic.h"
#include "src/simulation.hpp"

#include <cassert>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <chrono>
#include <random>

std::string botname = "Shummie";

bool alliance_active = true;
bool save_ally = false;

int main(int argc, char* argv[]) {
    if (argc == 2) {
        time_per_move = stoi(argv[1]);
    }
    hlt::Metadata metadata = hlt::initialize(botname);
    player_id = metadata.player_id;

	hlt::Map& initial_map = metadata.initial_map;
	// We now have 1 full minute to analyse the initial map.
	std::ostringstream initial_map_intelligence;
	initial_map_intelligence
            << "width: " << initial_map.map_width
            << "; height: " << initial_map.map_height
            << "; players: " << initial_map.ship_map.size()
            << "; my ships: " << initial_map.ship_map.at(player_id).size()
            << "; planets: " << initial_map.planets.size();
    hlt::Log::log(initial_map_intelligence.str());

    for (;;) {
        hlt::Map map = hlt::in::get_map();
        start_time = std::chrono::high_resolution_clock::now();
        last_turn_map = game_map;
        game_map = &map;
        ++turn;

        num_players = game_map->ships.size();
        hlt::Log::log("# of players: " + to_string(num_players), 5);

        if (!rush_mode) {
            // slight bug in that we may try to dock too early!
            simulate_start_of_turn();
        }

        populate_nearby_ships();

        create_filtered_vectors(player_id);
        assign_p_ship_num();

		process_last_turn_ship_data();
        if (turn > 1) {
            update_planet_data();
        }

        if (alliance_active) {
            check_ally_signal();
        }

        predict_enemy_moves();

        save_ally = false;
        
        if (ally_id != -1) {
            // need to recalculate vectors to ignore our ally.
            // when do we decide to attack the ally?
            hlt::Log::log("Ally found. recalculating...: Allyid: " + to_string(ally_id));
        
            // test if we should remain allies.
            bool still_ally = check_allied();
            if (still_ally) {                
                create_filtered_vectors_ally(player_id);

                // is our ally about to die?
                if (ally_planets.size() == 0) {
                    // Yes. Avoid them.
                    save_ally = true;

                    for (auto q : ally_undocked_ships) {
                        q->radius = hlt::constants::SHIP_RADIUS * 3 + hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED;
                        ally_docked_ships.push_back(q);
                    }
                }

            } else {
                ally_id = -1;
            }
        }

        score_all_planets();
		set_rush_mode();

        // debug messages:
        for (hlt::Ship* s : my_ships) {
            hlt::Log::log("Ship id: " + to_string(s->entity_id) + " x: " + to_string(s->location.pos_x) + " y: " + to_string(s->location.pos_y), 10);
        }

        if (num_players == 2) {
            int distract_count = 0;
            hlt::Log::log("Running 2 player logic.");

            int rush_mode_settle_counter = 0;
            if (rush_mode) {
                if (enemy_ships.size() < my_ships.size()) {
                    rush_mode_settle_counter = my_ships.size() - enemy_ships.size();
                    std::sort(my_ships.begin(), my_ships.end(), [](hlt::Ship* a, hlt::Ship* b) { return a->health < b->health; });
                }                
            }

            for (auto e : enemy_attacking_ships) {
                for (auto f : e->nearby_enemy_ships) {
                    if (f->owner_id != player_id || f->docking_status != hlt::ShipDockingStatus::Undocked || f->command != -1) { continue; }

                    f->command = 1;
                    f->behavior = default_behavior_2p;
                    f->entity_target = e;
                    // e->targeted.push_back(f);
                    f->target = e->location;
                    f->nav = dogfight;
                    break;
                }
            }
            
            for (hlt::Ship* s : my_ships) {

                if (s->command != -1) { continue; }

                if (enemy_ships.size() == 0) {
                    hlt::Log::log("No enemy ships alive next turn.");
                    break;
                }
				if (rush_mode) {
                    if (false && rush_mode_settle_counter > 0) {
                        if (rush_mode_settle_counter != 999) {
                            rush_mode_settle_counter = 999; // for now, just use 1 to settle.                        
                            s->behavior = settle;
                            if (!s->behavior->find_targets(s)) {
                                s->behavior = default_behavior_2p;
                                s->behavior->find_targets(s);
                            }
                        } else {
                            s->behavior = default_behavior_2p;
                            s->behavior->find_targets(s);
                        }
                    } else {
                        s->behavior = rush;
                        s->behavior->find_targets(s);
                    }
				} else {
                    // if (neutral_planets.size() > 0 && s->p_ship_num > 7 && ship_counter[s->entity_id] % 3 == 0) {
                    if (neutral_and_owned_planets.size() > 0 && s->p_ship_num > 7 && s->rand1 <= 0.15) {
						s->behavior = settle;
						s->behavior->find_targets(s);
                    } else if (s->p_ship_num > 25 && s->rand1 <= 0.30 && s->rand1 >= 0.15) {
                        // distractor?
						s->behavior = distractor;
						if (!s->behavior->find_targets(s)) {
							s->behavior = default_behavior_2p;
							s->behavior->find_targets(s);
						}
                        ++distract_count;
                    } else {						
                        s->behavior = defender;
                        if (!s->behavior->find_targets(s)) {
                            s->behavior = default_behavior_2p;
                            s->behavior->find_targets(s);
                        }
                    }
				}
            }
            hlt::Log::log("Initial moves complete.");

			// extra ships to a planet should set new destinations, furthest first.
			if (!rush_mode) {
                early_game_match_targets();
				reassign_ships_over_defense_capacity(2);
                int iter = 0;
                while (iter < 5 && reassign_ships_over_attack_capacity(std::max(2, int(my_undocked_ships.size() / (enemy_undocked_ships.size() + 1))))) {
                        iter++;
                }
                int attempts = 0;
				while(reassign_ships_over_docking_capacity() && attempts < 4) {
                    attempts++;
                }
			}

            hlt::Log::log("End of move assignment code");
        } else {
            // 4 player game
            hlt::Log::log("Running 4 player logic.");

            int distract_count = 0;
            bool survival_mode = determine_4p_survival_mode();

            int rush_mode_settle_counter = 0;
            if (rush_mode) {
                int enemy_ship_count = 0;
                for (auto q : enemy_ships) {
                    if (q->owner_id == rush_id) {
                        enemy_ship_count++;
                    }                    
                }
                if (enemy_ship_count < my_ships.size()) {
                    rush_mode_settle_counter = my_ships.size() - enemy_ship_count;
                    std::sort(my_ships.begin(), my_ships.end(), [](hlt::Ship* a, hlt::Ship* b) { return a->health < b->health; });
                }                
            }
			
			// check target..
			if (target_4p != -1) {
				// does this target still own any planets?
				bool still_alive = false;
				for (auto q : enemy_planets) {
					if (q->owner_id == target_4p) {
						still_alive = true;
						break;
					}
				}
				if (!still_alive) {
					target_4p = -1;
				}
			}
			
			if (target_4p == -1) {
				// pick the enemy that is closest to us?
				if (my_planets.size() > 0) {
					double closest_dist = 9999;
					hlt::Planet* closest_enemy_planet;
					for (auto p : my_planets) {						
						for (auto q : enemy_planets) {
							double dist = p->location.get_distance_to(q->location);
							if (dist < closest_dist) {
								closest_dist = dist;
								closest_enemy_planet = q;
							}
						}
					}
					if (closest_dist != 9999) {
						target_4p = closest_enemy_planet->owner_id;
					}
				}
			}

            if (!survival_mode) {
				
				for (auto e : enemy_attacking_ships) {
					for (auto f : e->nearby_enemy_ships) {
						if (f->owner_id != player_id || f->docking_status != hlt::ShipDockingStatus::Undocked || f->command != -1) { continue; }

						f->command = 1;
						f->behavior = default_behavior_2p;
						f->entity_target = e;
						// e->targeted.push_back(f);
						f->target = e->location;
						f->nav = dogfight;
						break;
					}
				}
				
                for (hlt::Ship* s : my_ships) {
                    if (enemy_ships.size() == 0) {
                        hlt::Log::log("No enemy ships alive next turn.");
                        break;
                    }
					if (s->command != -1) { continue; }
    				if (rush_mode) {
                        if (false && rush_mode_settle_counter > 0) {
                            rush_mode_settle_counter = 0; // for now, just use 1 to settle.
                            s->behavior = settle;
                            if (!s->behavior->find_targets(s)) {
                                s->behavior = default_behavior_2p;
                                s->behavior->find_targets(s);
                            }
                        } else {
                            s->behavior = rush_4p;
                            s->behavior->find_targets(s);
                        }
    				} else {
    					// if (false && neutral_planets.size() > 0 && s->p_ship_num > 15 && ship_counter[s->entity_id] % 5 == 0) {
                        if (neutral_and_owned_planets.size() > 0 && s->p_ship_num > 5 && s->rand1 <= 0.15) {
                            s->behavior = settle;
                            s->behavior->find_targets(s);
    					} else if (s->p_ship_num > 25 && s->rand1 <= 0.33 && s->rand1 >= 0.15) {
                            // distractor?
                            s->behavior = distractor_4p;
                            if (!s->behavior->find_targets(s)) {
                                s->behavior = default_behavior_2p;
                                s->behavior->find_targets(s);
                            }
                            ++distract_count;
                        } else {
    						if (s->p_ship_num % 1 == 0) {
                                s->behavior = defender;
    							if (!s->behavior->find_targets(s)) {

                                    s->behavior = default_behavior_2p;
                                    s->behavior->find_targets(s);
    							}
    						} else {
                                s->behavior = default_behavior_2p;
                                s->behavior->find_targets(s);
    						}
    					}
    				}
                }

                // extra ships to a planet should set new destinations, furthest first.
    			if (!rush_mode) {
                    early_game_match_targets();
    				reassign_ships_over_defense_capacity(2);
                    int iter = 0;
                    while (iter < 1 && reassign_ships_over_attack_capacity(std::max(3, int(my_undocked_ships.size() / (enemy_undocked_ships.size() + 1))))) {
                        iter++;
                    }
                    int attempts = 0;
                    while(reassign_ships_over_docking_capacity() && attempts < 4) {
                        attempts++;
                    }
    				if (counter[player_id] > 20) {
                        // Find a corner to sit in.
    					hide_ship_in_corner();
    				}
    			}
            } else { // survival_mode = true
                hlt::Log::log("Running survival mode 4p code.");
                for (hlt::Ship* s : my_ships) {
                    avoid_enemy(s);                    
                }
            }
            hlt::Log::log("End of move assignment code");
        }

        // Anti-rush code
        // Check if we should undock ships.
        if (my_docked_ships.size() > 0 && my_undocked_ships.size() <= 1 && enemy_undocked_ships.size() > 0 && my_ships.size() <= 4 && my_planets.size() == 1) {
            // check if the enemy's ships are within 5 from us.
            double undock = 0;
            for (hlt::Ship* e : enemy_undocked_ships) {
                for (hlt::Ship* s : my_docked_ships) {
                    if (s->location.get_distance_to(e->location) <= hlt::constants::MAX_SPEED * 8 + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2) {
                        undock++;
                        break;
                    }
                }
            }

            if (undock > 0) {
                for (hlt::Ship* s : my_docked_ships) {
                    if (s->docking_status == hlt::ShipDockingStatus::Undocking) {
                        undock--;
                    }
                }
				for (hlt::Planet* p : my_planets) {
					if (p->turns_to_next_ship() <= 6) {
						undock--;
					}
				}
            }

            if (undock - my_undocked_ships.size() > 0) {
                // start undocking ships!
                for (hlt::Ship* s : my_docked_ships) {
                    if (undock > 0) {
                        for (hlt::Ship* e : enemy_undocked_ships) {
                            if (s->location.get_distance_to(e->location) <= hlt::constants::MAX_SPEED * 7 + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2) {
                                s->command = 3;
                                undock--;
                                break;
                            }
                        }
                    }
                }

            }
        }

        // get moves based on each ship's target.
		// All move processing, including collision detection and post-processing occurs in the function below.

        hlt::Log::log("Recreating filtered vectors", 10);
        create_filtered_vectors(player_id); // reset filters from earlier target post-processing.

        if (enemy_ships.size() != 0) {
            nav_attempts = 0;
            hlt::Log::log("Running nav code");
            std::sort(my_undocked_ships.begin(), my_undocked_ships.end(), [](hlt::Ship* a, hlt::Ship* b) { return a->entity_id < b->entity_id;});
            hlt::navigation::determine_moves(true);
            hlt::Log::log("Initial moves done");
            //update_ship_attacks();
            while (nav_attempts <= 5 && (time_since_start() < time_per_move)) {
                hlt::navigation::determine_moves(false);
                hlt::Log::log("Finished determine_moves attempt: " + to_string(nav_attempts));				
                nav_attempts++;
                //update_ship_attacks();
            }
        }

        // modify for ally signal
        if (num_players == 4 && turn == 1 && alliance_active) {
            send_ally_signal();
        }

        if (num_players == 4 && alliance_active && ally_id != -1 && ally_planets.size() == 0 && enemy_planets.size() == 0 && neutral_planets.size() == 1 && enemy_ships.size() > 0 && ally_undocked_ships.size() > 0) {
            for (auto q : my_ships) {
                if (q->command == 2) {
                    q->command = -1;
                }
            }
        }

        // Log final moves and projected x/y for debugging purposes.
        hlt::Log::log("Final moves:");
        hlt::Log::log("-----------");
        for (hlt::Ship* s : my_ships) {
            hlt::Log::log("Ship id: " + to_string(s->entity_id) + " speed: " + to_string(s->speed) + " angle: " + to_string(s->angle));

            s->angle = (s->angle + 360) % 360;
            hlt::Log::log("Adjusted Ship id: " + to_string(s->entity_id) + " speed: " + to_string(s->speed) + " angle: " + to_string(s->angle));

			if (s->entity_target != NULL) {
				hlt::Log::log("entity target: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
			} else {
                hlt::Log::log("entity target: NULL target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
            }
            hlt::Log::log("vx: " + to_string(s->vx) + " vy: " + to_string(s->vy) + " proj_x: " + to_string(s->location.pos_x + s->vx) + " proj_y: " + to_string(s->location.pos_y + s->vy));
            if (s->behavior != NULL) {
                hlt::Log::log("behavior: " + to_string(s->behavior->name()));
            } else {
                hlt::Log::log("No assigned behavior");
            }
        }

        /*
        auto s_time = time_since_start_ns();
        hlt::Log::log("Attempting to copy map");
        hlt::Map map_copy = hlt::Map(*game_map);
        hlt::Log::log("Map copy - success");
        auto time_taken = time_since_start_ns() - s_time;
        hlt::Log::log("Time to copy: " + to_string(time_taken));
        
        my_ships.clear();

        for (auto& player_v_pair : map_copy.ships) {
            if (player_v_pair.first == player_id) {
                for (hlt::Ship& s : player_v_pair.second) {
                    if (s.projected_health > 0) {
                        my_ships.push_back(&s);
                    }
                }
            }
        }

        // testing turn simulation
        s_time = time_since_start_ns();
        hlt::Log::log("Attempting turn simulation");
        hlt::Map next_turn_map = simulate_turn(&map_copy);
        hlt::Log::log("Simulation - success");
        time_taken = time_since_start_ns() - s_time;
        hlt::Log::log("Time to simulate: " + to_string(time_taken));
        */

        if (!hlt::out::send_moves(my_ships)) {
            hlt::Log::log("send_moves failed; exiting");
            break;
        }

		hlt::Log::log("Saving last turn's ship information", 10);
        save_last_game_ships();

        /*
        for (int i = 0; i < num_players; ++i) {
            hlt::Log::log("Player number: " + to_string(i) + " Score: " + to_string(game_map->score(i)));
        }
        
        hlt::Log::log("Simulated scores:");
        for (int i = 0; i < num_players; ++i) {
            hlt::Log::log("Player number: " + to_string(i) + " Score: " + to_string(next_turn_map.score(i)));
        }
        */

        double total_time_taken = time_since_start_ns();
        if (total_time_taken > 1000000) {
            hlt::Log::log("Time elapsed: " + to_string(total_time_taken / 1000000) + " ms");
        } else {
            hlt::Log::log("Time elapsed: " + to_string(total_time_taken) + " ns");
        }

        
    }
}
