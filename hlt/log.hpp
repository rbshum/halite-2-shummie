#pragma once

#include <fstream>
#include <iostream>
#include <string>

const int VERBOSITY = 9;

namespace hlt {
    struct Log {
    private:
        std::ofstream file;

        void initialize(const std::string& filename) {
            file.open(filename, std::ios::trunc | std::ios::out);
        }

    public:
        static Log& get() {
            static Log instance{};
            return instance;
        }

        static void open(const std::string& filename) {
            get().initialize(filename);
        }

        static void log(const std::string& message) {
            get().file << message << std::endl;
        }

        static void log(const std::string& message, const int verbosity_msg) {
            if (verbosity_msg <= VERBOSITY) {
                get().file << message << std::endl;
            }
        }
    };
}
