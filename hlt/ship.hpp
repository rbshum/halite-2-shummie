#pragma once

#include "constants.hpp"
#include "types.hpp"
#include "planet.hpp"
// #include "../src/behavior.cpp"

class Navigation;
class Behavior;

namespace hlt {
	
	struct Planet;
	
    /// The states a ship can be in regarding docking.
    enum class ShipDockingStatus {
        Undocked = 0,
        Docking = 1,
        Docked = 2,
        Undocking = 3,
    };

    struct Ship : Entity {
        /// The turns left before the ship can fire again.
        int weapon_cooldown;

        ShipDockingStatus docking_status;

        /// The number of turns left to complete (un)docking.
        int docking_progress;

        /// The id of the planet this ship is docked to. Only valid if
        /// Ship::docking_status is -not- DockingStatus::Undocked.
        int docked_planet;

        int command;
		int p_ship_num; 	// This is a unique ID for each PLAYER. Basically, an index for the Nth ship produced by a player.

        hlt::Location target;
        hlt::Entity* entity_target;

        Navigation* nav;
		Behavior* behavior;

        std::vector<hlt::Ship*> nearby_enemy_ships;
        std::vector<hlt::Ship*> nearby_friendly_ships;

        std::vector<hlt::Ship*> enemy_ships_in_attack_range;

        double rand1;
        double rand2;

        bool kamakaze;

        /// Check if this ship is close enough to dock to the given planet.
        bool can_dock(const Planet& planet);

        Ship();

        Ship (const Ship& other);

        void update_ships_in_attack_range() {
            double attack_range = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
            for (hlt::Ship* q : nearby_enemy_ships) {
    			double dist = q->projected_location.get_distance_to(projected_location);
                if (dist <= attack_range) {
                    enemy_ships_in_attack_range.push_back(q);
                }
            }
        }

    };
}
