#pragma once

#include "map.hpp"
#include "util.hpp"
#include "globals.h"
#include "../src/logic.h"

namespace hlt {
    namespace navigation {
        bool determine_moves(bool initial);
    }
}
