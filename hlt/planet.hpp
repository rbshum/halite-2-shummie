#pragma once

#include <vector>
#include <algorithm>

#include "types.hpp"
#include "entity.hpp"
#include "ship.hpp"

namespace hlt {

    struct Ship;

    struct Planet : Entity {
        bool owned;
        bool frozen; // only for simulation purposes

        /// The remaining resources.
        int remaining_production;

        /// The currently expended resources.
        int current_production;

        /// The maximum number of ships that may be docked.
        int docking_spots;

        // The # of resources currently available for a ship.
        int resources;

        /// Contains IDs of all ships in the process of docking or undocking,
        /// as well as docked ships.
        std::vector<int> docked_ships_id;
		std::vector<Ship*> docked_ships;

        bool is_full() const {
            return int(docked_ships.size()) == docking_spots;
        }

        int turns_to_next_ship();

        Planet() : Entity() {
            owned = false;
            remaining_production = 0;
            current_production = 0;
            docking_spots = 0;
            resources = 0;
			type = 1;
            frozen = false;
        }

        Planet (const Planet& other) : Entity(other) {
            owned = other.owned;
            remaining_production = other.remaining_production;
            current_production = other.current_production;
            docking_spots = other.docking_spots;
            resources = other.resources;
            frozen = other.frozen;
			type = 1;
            for (auto& a : other.docked_ships_id) {
                docked_ships_id.push_back(a);
            }

            // docked ships needs to be set externally.
        }

        void remove_ship(hlt::Ship& ship);
        void add_ship(hlt::Ship& ship);

    };
}
