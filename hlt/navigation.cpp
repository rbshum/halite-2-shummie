#include "navigation.hpp"

bool hlt::navigation::determine_moves(bool initial) {

    bool all_ok = true;

    // check for collisions
    hlt::Log::log("Checking for collisions");
    std::vector<Entity*> targets_to_avoid;
    targets_to_avoid.reserve(all_planets.size() + enemy_docked_ships.size() + my_docked_ships.size() + my_undocked_ships.size() + enemy_undocked_ships.size());
    targets_to_avoid.insert(targets_to_avoid.end(), all_planets.begin(), all_planets.end());
    targets_to_avoid.insert(targets_to_avoid.end(), enemy_docked_ships.begin(), enemy_docked_ships.end());
    targets_to_avoid.insert(targets_to_avoid.end(), my_docked_ships.begin(), my_docked_ships.end());
    targets_to_avoid.insert(targets_to_avoid.end(), ally_docked_ships.begin(), ally_docked_ships.end());

    std::vector<Entity*> targets_to_avoid_kamakaze; // for now, we'll assume we still don't want to crash into a planet.
    targets_to_avoid_kamakaze.reserve(all_planets.size() + enemy_docked_ships.size() + my_docked_ships.size() + my_undocked_ships.size() + enemy_undocked_ships.size());
    targets_to_avoid_kamakaze.insert(targets_to_avoid_kamakaze.end(), all_planets.begin(), all_planets.end());
    targets_to_avoid_kamakaze.insert(targets_to_avoid_kamakaze.end(), my_docked_ships.begin(), my_docked_ships.end());
    targets_to_avoid_kamakaze.insert(targets_to_avoid_kamakaze.end(), ally_docked_ships.begin(), ally_docked_ships.end());

    if (!initial) {
        targets_to_avoid.insert(targets_to_avoid.end(), my_undocked_ships.begin(), my_undocked_ships.end());
        // targets_to_avoid.insert(targets_to_avoid.end(), enemy_undocked_ships.begin(), enemy_undocked_ships.end());

        targets_to_avoid_kamakaze.insert(targets_to_avoid_kamakaze.end(), my_undocked_ships.begin(), my_undocked_ships.end());

    }

    for (Ship* s : my_undocked_ships) {
        if (s->nav == NULL) { continue; }
        if (time_since_start() > time_per_move) {
            hlt::Log::log("Ran out of time! Time elapsed: " + to_string(time_since_start()));
            return true;
        }
        // if (s->entity_target == NULL) { continue; } // currently assumes ALL ships that want to move will set entity_target
        if (s->command != 1) {continue;}

        std::vector<std::tuple<int, int, double>> valid_moves;

        s->speed = 0;
        s->update();
        valid_moves.push_back(std::make_tuple(0, 0, s->nav->score_move(s, initial)));

        // for each possible speed/angle, need to see if there are any entities that we might crash into.
        for (int new_speed = 1; new_speed <= constants::MAX_SPEED; ++new_speed) {
            s->speed = new_speed;
            for (int angle_inc = 0; angle_inc < 360; ++angle_inc) {                        
                s->angle = angle_inc;
                s->update();

                if (s->location.pos_x + s->vx < 0 || s->location.pos_y + s->vy < 0 || s->location.pos_x + s->vx >= game_map->map_width || s->location.pos_y + s->vy >= game_map->map_height) {
                    continue;
                }

                valid_moves.push_back(std::make_tuple(s->speed, s->angle, s->nav->score_move(s, initial)));
            }
        }

        // start from best move and continue until we find a valid move.
        std::sort(valid_moves.begin(), valid_moves.end(), [](const std::tuple<int, int, double>& a, const std::tuple<int, int, double>& b) { return std::get<2>(a) > std::get<2>(b);});

        bool move_found = false;

        for (std::tuple<int, int, double>& move_tuple : valid_moves) {

            s->speed = std::get<0>(move_tuple);
            s->angle = std::get<1>(move_tuple);
            s->update();

            bool ok = true;

            if (!s->kamakaze) {
                for (Entity* e : targets_to_avoid) {
                    // if (e == s->entity_target && e->radius == 0.5 && e->owner_id != player_id) { continue; }
                    if (collision_check(s, e)) {
                        ok = false;
                        break;
                    }
                }
            } else {
                for (Entity* e : targets_to_avoid_kamakaze) {
                    // if (e == s->entity_target && e->radius == 0.5 && e->owner_id != player_id) { continue; }
                    if (collision_check(s, e)) {
                        ok = false;
                        break;
                    }
                }
            }

            if (ok) {
                move_found = true;
                break;
            }
        }

        // What happens if we don't find a valid move? Best option would be to stay still?
        if (!move_found) {
            hlt::Log::log("Error: No valid moves were found. Setting ship to STILL, ID: " + to_string(s->entity_id), 10);
            // set speed and velocities to 0.
            s->speed = 0;
            s->vx = 0;
            s->vy = 0;
            s->update();
            all_ok = false;
        }

        s->update_ships_in_attack_range();

        for (hlt::Ship* q : s->nearby_enemy_ships) {
            double d = hlt::constants::SHIP_RADIUS * 2 + hlt::constants::WEAPON_RADIUS;

            if (q->projected_location.get_distance_to(s->projected_location) <= d) {
                q->update_ships_in_attack_range();
            }
        }
    }

    return all_ok;

}