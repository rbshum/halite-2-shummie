#include "globals.h"

// hlt::PlayerId player_id;
int player_id;

int num_players;
int counter[4] = {0, 0, 0, 0};
std::unordered_map<int, int> ship_counter;
hlt::Map* last_turn_map;
hlt::Map* game_map;
int turn = 0;

std::unordered_map<int, hlt::Ship> last_turn_ships;

std::vector<hlt::Planet*> all_planets, enemy_planets, my_planets, neutral_planets, ally_planets;
std::vector<hlt::Planet*> my_planets_dockable;
std::vector<hlt::Planet*> neutral_and_owned_planets; // actually neutral & owned dockable planets.
std::vector<hlt::Planet*> interesting_planets;

std::vector<hlt::Ship*> all_ships, enemy_ships, my_ships, ally_ships;
std::vector<hlt::Ship*> enemy_undocked_ships, enemy_docked_ships, my_undocked_ships, my_docked_ships, ally_docked_ships, ally_undocked_ships;
std::vector<hlt::Ship*> enemy_attacking_ships;

std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
double time_per_move = 1750;

bool rush_mode = false;
int rush_id = -1;
int ally_id = -1;
int target_4p = -1;

int nav_attempts = 0;

std::random_device rd;
std::mt19937 eng(rd());
std::uniform_real_distribution<double> random_real(0, 1);

Navigation* default_navigation = new Default_Navigation;
Navigation* navigate_to_planet = new Navigate_To_Planet;
Navigation* navigate_to_planet_4p_rush = new Navigate_To_Planet_4p_Rush;
Navigation* coward = new Coward;
Navigation* retreat_from_all = new Retreat_From_All;
Navigation* retreat_from_all_group_friendly = new Retreat_From_All_Group_Friendly;
Navigation* attack_docked_ship = new Attack_Docked_Ship;
Navigation* dogfight = new Dogfight;
Navigation* dogfight_rush = new Dogfight_Rush;
Navigation* dogfight_rush_4p = new Dogfight_Rush_4p;
Navigation* attack_docked_ship_avoid_all = new Attack_Docked_Ship_Avoid_All;
Navigation* defense = new Defense;

Behavior* default_behavior_2p = new Default_Behavior_2p;
Behavior* default_behavior_4p = new Default_Behavior_4p;
Behavior* rush = new Rush;
Behavior* rush_4p = new Rush_4p;
Behavior* settle = new Settle;
Behavior* distractor = new Distractor;
Behavior* distractor_4p = new Distractor_4p;
Behavior* defender = new Defender;

double time_since_start() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time).count();
}

double time_since_start_ns() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start_time).count();
}

int stoi(std::string s) //The conversion function
{
    return atoi(s.c_str());
}


std::pair<double, double> closest(double x1, double y1, double x2, double y2) {
    double da = y2 - y1;
    double db = x1 - x2;
    double c1 = da * x1 + db * y1;
    double c2 = 0;  //  -db * this.x + da * this.y
    double det = da * da + db * db;
    double cx = 0;
    double cy = 0;

    if (det != 0) {
        cx = (da * c1 - db * c2) / det;
        cy = (da * c2 + db * c1) / det;
    }

    std::pair <double, double> ret_pair = std::make_pair(cx, cy);

    return ret_pair;
}


bool collision_check(hlt::Entity* s1, hlt::Entity* s2) {

    double dist = (s1->location.pos_x - s2->location.pos_x) * (s1->location.pos_x - s2->location.pos_x) + (s1->location.pos_y - s2->location.pos_y) * (s1->location.pos_y - s2->location.pos_y);

    double sr = (s1->radius + s2->radius + 0.01) * (s1->radius + s2->radius + 0.01);
    // double sr = (s1->radius + s2->radius) * (s1->radius + s2->radius);

    if (dist < sr) {
        // Technically should return true, but if we're going to immediately collide, there's nothing we can do. and if so, technically we should probably have crashed last turn
        return false;
    }

    if (s1->vx == s2->vx && s1->vy == s2->vy) {
        // objects with the same velocity will never collide
        return false;
    }

    double dx = s1->location.pos_x - s2->location.pos_x;
    double dy = s1->location.pos_y - s2->location.pos_y;
    double dvx = s1->vx - s2->vx;
    double dvy = s1->vy - s2->vy;

    auto cp = closest(dx, dy, dx + dvx, dy + dvy);

    double pdist = cp.first * cp.first + cp.second * cp.second;
    double mypdist = (dx - cp.first) * (dx - cp.first) + (dy - cp.second) * (dy - cp.second);

    if (pdist < sr) {
        double length = sqrt(dvx * dvx + dvy * dvy);
        double backdist = sqrt(sr - pdist);

        double cp0 = cp.first - backdist * (dvx / length);
        double cp1 = cp.second - backdist * (dvy / length);

        double mypd2 = (dx - cp0) * (dx - cp0) + (dy - cp1) * (dy - cp1);

        if (mypd2 > mypdist) {
            return false;
        }

        if (sqrt(mypd2) > length) {
            return false;
        }

        return true;
    }

    return false;

}

bool might_attack(hlt::Ship* s1, hlt::Ship* s2) {

    // s2 is the stationary entity.

    double dist = (s1->location.pos_x - s2->location.pos_x) * (s1->location.pos_x - s2->location.pos_x) + (s1->location.pos_y - s2->location.pos_y) * (s1->location.pos_y - s2->location.pos_y);

    double s2r = s2->radius + hlt::constants::WEAPON_RADIUS;
    if (s2->docking_status == hlt::ShipDockingStatus::Undocked) {
        s2r += hlt::constants::MAX_SPEED;
    }
    double sr = (s1->radius + s2r) * (s1->radius + s2r);
    // double sr = (s1->radius + s2->radius) * (s1->radius + s2->radius);

    if (dist < sr) {
        // Technically should return true, but if we're going to immediately collide, there's nothing we can do. and if so, technically we should probably have crashed last turn
        return true;
    }

    if (s1->vx == 0 && s1->vy == 0) {
        // objects with the same velocity will never collide
        return false;
    }

    double dx = s1->location.pos_x - s2->location.pos_x;
    double dy = s1->location.pos_y - s2->location.pos_y;
    double dvx = s1->vx;
    double dvy = s1->vy;

    auto cp = closest(dx, dy, dx + dvx, dy + dvy);

    double pdist = cp.first * cp.first + cp.second * cp.second;
    double mypdist = (dx - cp.first) * (dx - cp.first) + (dy - cp.second) * (dy - cp.second);

    if (pdist < sr) {
        double length = sqrt(dvx * dvx + dvy * dvy);
        double backdist = sqrt(sr - pdist);

        double cp0 = cp.first - backdist * (dvx / length);
        double cp1 = cp.second - backdist * (dvy / length);

        double mypd2 = (dx - cp0) * (dx - cp0) + (dy - cp1) * (dy - cp1);

        if (mypd2 > mypdist) {
            return false;
        }

        if (sqrt(mypd2) > length) {
            return false;
        }

        return true;
    }

    return false;

}


std::pair<bool, double> collision_time(hlt::Entity* s1, hlt::Entity* s2) {

    double r = s1->radius + s2->radius + 0.0005;

    double dx = s1->location.pos_x - s2->location.pos_x;
    double dy = s1->location.pos_y - s2->location.pos_y;
    double dvx = s1->vx - s2->vx;
    double dvy = s1->vy - s2->vy;

    double a = std::pow(dvx, 2) + std::pow(dvy, 2);
    double b = 2 * (dx * dvx + dy * dvy);
    double c = std::pow(dx, 2) + std::pow(dy, 2) - std::pow(r, 2);

    double disc = b * b - 4 * a * c;

    if (a == 0.0) {
        if (b == 0.0) {
            if (c <= 0.0) {
                // implies sr^2 >= dx^2 + dy^2 and the two are already colliding
                return {false, 0.0};
                // return true; <-- if already colliding, then doesn't really matter and can't do anything about it.
            }
            return {false, 0.0};
        }
        double t = -c / b;
        if (t >= 0.0) {
            return {true, t};
        }
        return {false, 0.0};
    } else if (disc == 0.0) {
        // 1 solution
        double t = -b / (2 * a);
        return {true, t};
    } else if (disc > 0) {

        double t1 = -b + std::sqrt(disc);
        double t2 = -b - std::sqrt(disc);

        if (t1 >= 0.0 && t2 >= 0.0) {
            return {true, std::min(t1, t2) / (2 * a)};
        } else if (t1 <= 0.0 && t2 <= 0.0) {
            return {true, std::max(t1, t2) / (2 * a)};
        } else {
            return {true, 0.0};
        }
    } else {
        return {false, 0.0};
    }
}

bool collision_check_2(hlt::Entity* s1, hlt::Entity* s2) {

    double dist = s1->location.get_distance_to(s2->location);
    if (dist > s1->speed + s2->speed + s1->radius + s2->radius) {
        return false;
    }


    auto t = collision_time(s1, s2);
    if (t.first) {
        if (t.second >= 0 && t.second <= 1) {
            return true;
        }
    }
    return false;

}
