#include "ship.hpp"

/// Check if this ship is close enough to dock to the given planet.
bool hlt::Ship::can_dock(const Planet& planet) {
	return location.get_distance_to(planet.location) <= (constants::DOCK_RADIUS + planet.radius + this->radius);
}

hlt::Ship::Ship() : Entity() {
	weapon_cooldown = 0;
	docking_progress = -1;
	docked_planet = -1;
	command = -1;
	p_ship_num = -1;
	entity_target = NULL;
	nav = NULL;
	behavior = NULL;
	type = 2;
	rand1 = -1;
	rand2 = -1;
	kamakaze = false;
}

hlt::Ship::Ship (const Ship& other) : Entity(other) {
	weapon_cooldown = other.weapon_cooldown;
	docking_status = other.docking_status;
	docking_progress = other.docking_progress;
	docked_planet = other.docked_planet;
	command = other.command;
	p_ship_num = other.p_ship_num;
	type = 2;
	rand1 = other.rand1;
	rand2 = other.rand2;
	// I suck at this. lets just set the location parameter manually

	target.pos_x = other.location.pos_x;
	target.pos_y = other.location.pos_y;
	
	nav = other.nav;
	behavior = other.behavior;

	kamakaze = other.kamakaze;
	// entity_target needs to be set externally.
	entity_target = NULL;
	
}