#include "map.hpp"

template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;

  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}

namespace hlt {
    Map::Map() {
        // default constructor. should not be called to actually create an object in general.        
    }
    
    Map::Map(const int width, const int height) : map_width(width), map_height(height) {
    }

    Map::Map (const Map& other) {
        // handles the deep copying and setting of ships/planets

        map_height = other.map_height;
        map_width = other.map_width;
        
        // copy the base planet and ship objects first. then worry about setting new pointers
        for (int i = 0; i < int(other.planets.size()); ++i) {
            hlt::Planet p = Planet(other.planets[i]);
            planets.push_back(p);
            planet_map[p.entity_id] = i;
        }

        for (int i = 0; i < int(other.ships.size()); ++i) {
            std::vector<hlt::Ship>& ship_vec = ships[i];
            entity_map<int>& ship_e_map = ship_map[i];

            int n_ships = other.ships.at(i).size();
            ship_vec.reserve(n_ships);
            for (int j = 0; j < n_ships; ++j) {
                hlt::Ship s = Ship(other.ships.at(i).at(j));
                ship_vec.push_back(s);
                ship_e_map[s.entity_id] = j;
            }
        }

        // ok now all ships and plants have been repopulated into the map. we need to set some values in ships and planets.
        // populate planet docked_ships
        for (hlt::Planet& p : planets) {
            if (p.owned) {
                for (int id : p.docked_ships_id) {
                    hlt::Ship& s = get_ship(p.owner_id, id);
                    p.docked_ships.push_back(&s);
                }
            }
        }

        // populate entity target for each ship
        for (int i = 0; i < int(other.ships.size()); ++i) {
            for (const hlt::Ship& os : other.ships.at(i)) {			
                if (os.entity_target != NULL) {
					hlt::Ship& my_s = get_ship(os.owner_id, os.entity_id);
					hlt::Entity* tgt;
					if (os.entity_target->type == 2) {
						tgt = &get_ship(os.entity_target->owner_id, os.entity_target->entity_id);
					} else {
						// should be a planet then
						tgt = &get_planet(os.entity_target->entity_id);
					}
					my_s.entity_target = tgt;
                }
				
				if (os.targeted.size() != 0) {
					hlt::Ship& my_s = get_ship(os.owner_id, os.entity_id);
					for (const auto& e : os.targeted) {
						hlt::Ship* tgt = &get_ship(e->owner_id, e->entity_id);							
						my_s.targeted.push_back(tgt);
					}
					
				}
            }
        }

		for (const hlt::Planet& op : other.planets) {
			if (op.targeted.size() != 0) {
				hlt::Planet& my_p = get_planet(op.entity_id);
				for (const auto& e : op.targeted) {
					hlt::Ship* tgt = &get_ship(e->owner_id, e->entity_id);
					my_p.targeted.push_back(tgt);
				}
			}
		}
    }

    double Map::score(int player_num) {
        // calculates the score for a given player.

        static double SCORE_OWNED_PLANET = 200;
        static double SCORE_PER_UNDOCKED_SHIP = 75;
        

        double score = 0;

        for (hlt::Planet& p : planets) {
            if (p.owner_id == player_num) {
                score += SCORE_OWNED_PLANET;
            }
        }

        for (hlt::Ship& s : ships[player_num]) {
            if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                score += SCORE_PER_UNDOCKED_SHIP;
            }
            score += s.health;
        }

        return score;

    }

}

