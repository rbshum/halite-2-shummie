#pragma once

#include "location.hpp"
#include "types.hpp"

namespace hlt {
	struct Ship;	
	
    struct Entity {
        int entity_id;
        int owner_id;
        Location location;
        int health;
        int projected_health;
        double radius;

        double score_1;
		double score_2; // used for settling score.
        double vx, vy;
        int speed, angle;
		
		int type; // 1 = planet, 2 = ship

        int tag = 0; // used for random stuff -- such as planet visiting.
					
		std::vector<Ship*> targeted; // A list of ships that are targeting this ship (friendly only??)
		
		Location projected_location; // The projected location for this entity given current speed/angle. Assumes no collision.

        Entity() {
            entity_id = -1;
            owner_id = -999;
            location.pos_x = -1;
            location.pos_y = -1;
            health = -1;
            projected_health = -1;
            radius = 0;
            score_1 = -1;
			score_2 = -1;
            vx = 0;
            vy = 0;
            speed = 0;
            angle = 0;
            projected_location.pos_x = -1;
            projected_location.pos_y = -1;
			type = 0;
            tag = 0;
        }

        Entity(const Entity& other) {
            entity_id = other.entity_id;
            owner_id = other.owner_id;
            location.pos_x = other.location.pos_x;
            location.pos_y = other.location.pos_y;
            health = other.health;
            projected_health = other.projected_health;
            radius = other.radius;
            score_1 = other.score_1;
			score_2 = other.score_2;
            vx = other.vx;
            vy = other.vy;
            speed = other.speed;
            angle = other.angle;
            projected_location.pos_x = other.projected_location.pos_x;
            projected_location.pos_y = other.projected_location.pos_y;
			type = other.type;
            tag = other.tag;
        }

        bool is_alive() const {
            return health > 0;
        }
		
		void update() {
			// should be called everytime speed or angle is updated.
			// recalculates the vx/vy positions. 
			vx = cos(angle * M_PI / 180) * speed;
			vy = sin(angle * M_PI / 180) * speed;
			
			projected_location.pos_x = location.pos_x + vx;
			projected_location.pos_y = location.pos_y + vy;
		}
		
    };
}
