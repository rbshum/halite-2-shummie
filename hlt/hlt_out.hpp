#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "log.hpp"

namespace hlt {
    namespace out {
        static bool send_string(const std::string& text) {
            // note that std::endl flushes
            std::cout << text << std::endl;
            return !std::cout.bad();
        }

        static bool send_moves(const std::vector<Ship*>& ships) {
            std::ostringstream oss;
            for (Ship* s : ships) {
                switch (s->command) {
                    case -1:
                        break;
                    case 3: // undock
                        oss << "u " << s->entity_id << " ";
                        break;
                    case 2: // dock
                        oss << "d " << s->entity_id << " " << s->entity_target->entity_id << " ";
                        break;
                    case 1:
                        int s_angle = s->angle % 360;
                        if (s_angle < 0) {
                            s_angle += 360;
                        }
                        oss << "t " << s->entity_id << " " << int(s->speed) << " " << s_angle << " ";
                        break;
                }
            }
            return send_string(oss.str());
        }
    }
}
