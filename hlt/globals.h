#pragma once


#include <unordered_map>
#include <algorithm>
#include "types.hpp"
#include "../src/behavior.h"
#include "hlt.hpp"
#include "planet.hpp"
#include "ship.hpp"
#include "entity.hpp"
#include "map.hpp"


#include <chrono>
#include <random>
#include <vector>

// extern hlt::PlayerId player_id;
extern int player_id;
extern std::vector<hlt::Planet*> all_planets, enemy_planets, my_planets, neutral_planets, ally_planets;
extern std::vector<hlt::Planet*> my_planets_dockable;
extern std::vector<hlt::Planet*> neutral_and_owned_planets; // actually neutral & owned dockable planets.
extern std::vector<hlt::Planet*> interesting_planets;

extern std::vector<hlt::Ship*> all_ships, enemy_ships, my_ships, ally_ships;
extern std::vector<hlt::Ship*> enemy_undocked_ships, enemy_docked_ships, my_undocked_ships, my_docked_ships, ally_docked_ships, ally_undocked_ships;
extern std::vector<hlt::Ship*> enemy_attacking_ships;

extern std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
extern double time_per_move;

extern int stoi(std::string s);
extern double time_since_start();
extern double time_since_start_ns();

extern bool collision_check(hlt::Entity* s1, hlt::Entity* s2);
extern bool might_attack(hlt::Ship* s1, hlt::Ship* s2);
extern bool collision_check_old(hlt::Entity* s1, hlt::Entity* s2);

extern std::pair<double, double> closest(double x1, double y1, double x2, double y2);
extern std::pair<bool, double> collision_time(hlt::Entity* s1, hlt::Entity* s2);

extern int num_players;
extern hlt::Map* last_turn_map;
extern hlt::Map* game_map;
extern int turn;
extern int counter[4];
extern std::unordered_map<int, int> ship_counter;
extern std::unordered_map<int, hlt::Ship> last_turn_ships;

extern bool rush_mode;
extern int rush_id;
extern int ally_id;
extern int target_4p;

extern int nav_attempts;

extern std::random_device rd;
extern std::mt19937 eng;
extern std::uniform_real_distribution<double> random_real;

extern Navigation* default_navigation;
extern Navigation* navigate_to_planet;
extern Navigation* coward;
extern Navigation* navigate_to_planet_4p_rush;
extern Navigation* retreat_from_all;
extern Navigation* retreat_from_all_group_friendly;
extern Navigation* attack_docked_ship;
extern Navigation* dogfight;
extern Navigation* dogfight_rush;
extern Navigation* dogfight_rush_4p;
extern Navigation* attack_docked_ship_avoid_all;
extern Navigation* defense;

extern Behavior* default_behavior_2p;
extern Behavior* default_behavior_4p;
extern Behavior* rush;
extern Behavior* rush_4p;
extern Behavior* settle;
extern Behavior* distractor;
extern Behavior* distractor_4p;
extern Behavior* defender;
