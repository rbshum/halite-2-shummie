#include "planet.hpp"

int hlt::Planet::turns_to_next_ship() {
    // calculates the expected # of turns until the next ship is produced. Assumes that there are no add'l docked ships.
    int resources_needed = 72 - resources;
    int ships_docked = 0;
    int num_docked_max = 0;
    for (Ship* s : docked_ships) {
        if (s->docking_status == ShipDockingStatus::Docked) {
            ++ships_docked;
            ++num_docked_max;
        } else if (s->docking_status == hlt::ShipDockingStatus::Docking) {
            ++num_docked_max;
        }
    }

    int turns = 0;

    while (resources_needed > 0 && turns < 10) {
        ++turns;
		resources_needed -= ships_docked * hlt::constants::BASE_PRODUCTIVITY;
        if (ships_docked != num_docked_max) {
            for (Ship* s : docked_ships) {
                if (s->docking_progress == turns) {
                    ++ships_docked;
                }
            }
        }
    }

    return turns;
}

void hlt::Planet::remove_ship(hlt::Ship& ship) {
    int ship_id = ship.entity_id;
    auto pos = std::find(docked_ships_id.begin(), docked_ships_id.end(), ship_id);
    if (pos != docked_ships_id.end()) {
        docked_ships_id.erase(pos);
    }
    if (docked_ships_id.size() == 0) {
        owned = false;
        owner_id = -1;
    }

    auto pos2 = std::find(docked_ships.begin(), docked_ships.end(), &ship);
    if (pos2 != docked_ships.end()) {
        docked_ships.erase(pos2);
    }
}

void hlt::Planet::add_ship(hlt::Ship& ship) {
    // assert(docked_ships.size() < docking_spots);
    docked_ships_id.push_back(ship.entity_id);
    docked_ships.push_back(&ship);
}
