#pragma once

#include "map.hpp"
#include "types.hpp"
#include "ship.hpp"
#include "planet.hpp"
#include "log.hpp"
#include <iostream>
#include <sstream>

namespace hlt {
    class Map {
    public:
        int map_width, map_height;

        std::unordered_map<int, std::vector<Ship>> ships;
        std::unordered_map<int, entity_map<int>> ship_map;

        std::vector<Planet> planets;
        entity_map<int> planet_map;

        Map();
        Map(int width, int height);
        Map(const Map& other);

        Ship& get_ship(const int player_id, int ship_id) {
            return ships.at(player_id).at(ship_map.at(player_id).at(ship_id));
        }

        Ship& get_ship(const int entity_id) {
            // need to loop through all ships to find it.
            for (auto& v_ships : ships) {
                for (auto& s : v_ships.second) {
                    if (s.entity_id == entity_id) {
                        return s;
                    }
                }
            }
        }

        Planet& get_planet(const int planet_id) {
            return planets.at(planet_map.at(planet_id));
        }

        double score(int player_num);
        



    };
}
