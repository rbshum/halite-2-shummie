#include "logic.h"

void move_or_dock_to_planet(hlt::Ship* s, hlt::Planet* p, double buffer=0.7) {
	if (s->can_dock(*p)) {
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " docking to planet");
		s->command = 2;
		s->entity_target = p;
		s->nav = default_navigation;
		p->targeted.push_back(s);
	} else {
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " navigating to planet");
		s->entity_target = p;
		s->target = p->location;
		s->command = 1;
		s->nav = navigate_to_planet;
		p->targeted.push_back(s);
	}
}

bool reassign_ships_over_docking_capacity() {
	// We don't need to send ships over the docking capacity of a planet (what about planets near the combat border??)
	// Take ships over the border and reassign them to other planets. Note, calling this function WILL modify existing planet vectors.

	hlt::Log::log("Checking to see if ships need to be reassigned to other planets.", 9);

	// For some reason erase/remove_if doesn't modify the vector properly. so we'll just loop through it ourself.
	hlt::Log::log("size of neutral_and_owned_planets: " + to_string(neutral_and_owned_planets.size()), 9);
	std::vector<hlt::Planet*> new_no_planets;
	std::vector<hlt::Planet*> overloaded_planets;
	for (hlt::Planet* p : neutral_and_owned_planets) {
		int num_extra = p->docking_spots - p->docked_ships.size() - p->targeted.size();
		if (num_extra > 0) {
			new_no_planets.push_back(p);
		} else {
			overloaded_planets.push_back(p);
		}
	}
	hlt::Log::log("size of new_no_planets: " + to_string(new_no_planets.size()), 9);
	if (new_no_planets.size() == neutral_and_owned_planets.size()) {
		hlt::Log::log("No planets assigned over capacity.", 9);
        return false;
	}
	// neutral_and_owned_planets = new_no_planets;
	neutral_and_owned_planets.clear();
	for (auto a : new_no_planets) {
		neutral_and_owned_planets.push_back(a);
	}

	hlt::Log::log("size of neutral_and_owned_planets after clear: " + to_string(neutral_and_owned_planets.size()), 9);

	hlt::Log::log("size of neutral_planets: " + to_string(neutral_planets.size()), 9);
	std::vector<hlt::Planet*> new_n_planets;
	for (hlt::Planet* p : neutral_planets) {
		int num_extra = p->docking_spots - p->docked_ships.size() - p->targeted.size();
		if (num_extra > 0) {
			new_n_planets.push_back(p);
		}
	}
	neutral_planets = new_n_planets;
	hlt::Log::log("size of neutral_planets after clear: " + to_string(neutral_planets.size()), 9);

	hlt::Log::log("size of interesting_planets: " + to_string(interesting_planets.size()));
	interesting_planets.clear();
	interesting_planets.insert(interesting_planets.end(), neutral_and_owned_planets.begin(), neutral_and_owned_planets.end());
	interesting_planets.insert(interesting_planets.end(), enemy_planets.begin(), enemy_planets.end());
	hlt::Log::log("size of interesting_planets after clear: " + to_string(interesting_planets.size()));

	for (hlt::Planet* p : all_planets) {
	
		if (p->tag == 667) { continue; } // this is an ugly ugly hack... but we dont' want to revisit planets we've already done.
		// time check
		if (p->owned && p->owner_id != player_id) {
			continue;
		}
		
		hlt::Log::log("checking planet id: " + to_string(p->entity_id) + " docking spots: " + to_string(p->docking_spots) + " docked_ship.size(): " + to_string(p->docked_ships.size()) + " targeted.size(): " + to_string(p->targeted.size()));

		int num_extra = p->docking_spots - p->docked_ships.size() - p->targeted.size();

		if (num_extra < 0) {
			hlt::Log::log("Planet id: " + to_string(p->entity_id) + " has " + to_string(num_extra) + " ships to reassign");
			hlt::Log::log("docking_spots: " + to_string(p->docking_spots) + " docked_ships.size " + to_string(p->docked_ships.size()) + " targeted: " + to_string(p->targeted.size()));
			std::sort(p->targeted.begin(), p->targeted.end(), [&](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(p->location) > b->location.get_distance_to(p->location);});
			hlt::Log::log("sorting complete");			

			// Sort backwards for distance. Ships furthest away from this planet should be reassigned.

			for (int i = 0; i < num_extra * -1; ++i) {
				// timeout check
				hlt::Ship* s = p->targeted[i];
				hlt::Log::log("Reassigning ship id: " + to_string(s->entity_id));
				s->command = -1;
				// Is there a way to be more generic on what function we're calling here?
                if (num_players == 2) {
                    s->behavior = default_behavior_2p;
                } else {
                    s->behavior = default_behavior_4p;
					s->behavior = default_behavior_2p;
                }

                s->behavior->find_targets(s);
				// find_move(s, 4);
			}
		}
	}

	for (auto a : overloaded_planets) { 
		a->tag = 667;
	}
    return true;
}

void set_rush_mode() {
	// determine if we should activate rush mode
    if (turn <= 16 && num_players == 2 && enemy_planets.size() == 0 && enemy_docked_ships.size() == 0) {
        for (hlt::Ship* ship_me : my_ships) {
            for (hlt::Ship* ship_enemy: enemy_ships) {
                if (ship_me->location.get_distance_to(ship_enemy->location) <= 11 * hlt::constants::MAX_SPEED) {
                    hlt::Log::log("Rush mode activated.");
                    rush_mode = true;
                }
            }
        }
		
		// If our ships are not close enough to trigger rush mode. don't.
		if (rush_mode) {
			// For example, we have a single ship heading to the center that finds another ship. If our other ships are far enough away, we don't want to recall them.
			double threshold = hlt::constants::MAX_SPEED * 3;
			for (hlt::Ship* s : my_ships) {
				for (hlt::Ship* r : my_ships) {
					double dist = s->location.get_distance_to(r->location);
					if (dist > threshold) {
						rush_mode = false;
						break;
					}
				}
			}
			
		}
		
    }

    // do we deactivate rush mode?
    if (rush_mode && num_players == 2) {
        bool keep_rush = false;
        for (hlt::Ship* ship_me : my_ships) {
            for (hlt::Ship* ship_enemy: enemy_ships) {
                if (ship_me->location.get_distance_to(ship_enemy->location) <= 11 * hlt::constants::MAX_SPEED) {
                    keep_rush = true;
                    break;
                }
                if (keep_rush) {break;}
            }
        }

        if (!keep_rush) {
            hlt::Log::log("Rush mode deactivated.");
            rush_mode = false;
        }
    }

	if (false && num_players == 4) {
		// Deactivate rush mode for 4 players. looks like it performs slightly worse. Code below is kept in case we can salvage it later.
		rush_mode = false;
		return;
	}

	// 4 player rush mode???
    // just cycle through all ships.
    if (turn <= 15 && num_players == 4 && rush_id == -1 && my_planets.size() == 0) {
        for (hlt::Ship* ship_me : my_ships) {
            for (hlt::Ship* ship_enemy: enemy_ships) {
                if (ship_me->location.get_distance_to(ship_enemy->location) <= 7 * hlt::constants::MAX_SPEED) {
                    hlt::Log::log("4 player rush mode activated");
                    rush_mode = true;
                    rush_id = ship_enemy->owner_id;
                    break;
                }
            }
            if (rush_mode) { break; }
        }
    }

	// do we deactivate rush mode?
	if (rush_mode && num_players == 4) {

		// if we've been producing ships...
		if (my_ships.size() >= 5) { 
			rush_mode = false;
		}

		auto i = game_map->ships.find(rush_id);
		if ((i == game_map->ships.end()) || i->second.size() == 0) {
			hlt::Log::log("4 player Rush mode deactivated.");
			rush_mode = false;
		} else {
            bool keep_rush = false;
            for (hlt::Ship* ship_me : my_ships) {
                for (hlt::Ship* ship_enemy: enemy_ships) {
                    if (ship_enemy->owner_id == rush_id && ship_me->location.get_distance_to(ship_enemy->location) <= 10 * hlt::constants::MAX_SPEED) {
                        keep_rush = true;
                        break;
                    }
                }
                if (keep_rush) { break; }
            }

            if (!keep_rush) {
                hlt::Log::log("4 player Rush mode deactivated.");
                rush_mode = false;
            }

			for (hlt::Ship* ship_me : my_ships) {
				hlt::Ship* e = get_closest_enemy_ship(ship_me);
				if (e->owner_id != rush_id) {
					hlt::Log::log("4 player Rush mode deactivated. Another enemy is closer.");
					// rush_mode = false;
				}
			}
		}
	}
}

void early_game_match_targets() {
	// In the early early game, we should try to target the same planet instead of splitting (assuming that the targeted planet has capacity).
	// If the targeted planet only has capacity 2, the later call to reassign_ships_over_docking_capacity should self-correct.

	if (turn < 10 && my_ships.size() == 3 && my_planets.size() == 0 && !rush_mode && my_ships[0]->entity_target != NULL) {
		hlt::Log::log("select same targets as ship 0");
		for (hlt::Ship* s : my_ships) {
            // if (s->p_ship_num == 2) { continue; }
			s->entity_target = my_ships[0]->entity_target;
			// s->target = s->location.get_closest_point(s->entity_target->location, s->entity_target->radius, 0.5 + random_real(eng) * 4);
			s->target = my_ships[0]->target;
		}

		hlt::Planet* target_p;
		for (hlt::Planet* p : all_planets) {
			if (std::find(p->targeted.begin(), p->targeted.end(), my_ships[0]) != p->targeted.end()) {
				target_p = p;
				break;
			}
		}

		for (hlt::Planet* p : all_planets) {
            //if (my_ships[1]->p_ship_num != 2) {
    			auto it1 = std::find(p->targeted.begin(), p->targeted.end(), my_ships[1]);
    			if (it1 != p->targeted.end()) {
    				std::swap(*it1, p->targeted.back());
    				p->targeted.pop_back();
    				target_p->targeted.push_back(my_ships[1]);
    			}
            //}

            //if (my_ships[2]->p_ship_num != 2) {
    			auto it2 = std::find(p->targeted.begin(), p->targeted.end(), my_ships[2]);
    			if (it2 != p->targeted.end()) {
    				std::swap(*it2, p->targeted.back());
    				p->targeted.pop_back();
    				target_p->targeted.push_back(my_ships[2]);
    			}
            //}

		}
	}
}

std::vector<hlt::Ship*> get_all_friendly_undocked_ships_in_range_not_targeting_friendly(hlt::Ship* s, double distance=hlt::constants::MAX_SPEED) {
    std::vector<hlt::Ship*> nearby_ships;

    // We MIGHT be able to assume that enemy_ships is sorted by distance already... But, why take that chance?
    for (hlt::Ship* e : my_ships) {
        if (e->docking_status == hlt::ShipDockingStatus::Undocked && e->location.get_distance_to(s->location) <= distance) {
            nearby_ships.push_back(e);
        }
    }

    return nearby_ships;
}

void reassign_ships_over_defense_capacity(int cap) {

	for (hlt::Planet* p : my_planets) {
		
		std::vector<hlt::Ship*> defending_ships;
		// count how many ships are defending this planet.
		for (hlt::Ship* s : p->docked_ships) {
			for (hlt::Ship* d : s->targeted) {
				defending_ships.push_back(d);
			}
		}
		
		double rad_check = p->radius + hlt::constants::DOCK_RADIUS + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 + hlt::constants::MAX_SPEED * 2;
		int sort_size = std::min(defending_ships.size(), enemy_undocked_ships.size());		
		std::partial_sort(enemy_undocked_ships.begin(), enemy_undocked_ships.begin() + sort_size, enemy_undocked_ships.end(), [p](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(p->location) < b->location.get_distance_to(p->location);});
		int enemy_count = 0;
		
		for (int i = 0; i < sort_size; ++i) {
			if (enemy_undocked_ships[i]->location.get_distance_to(p->location) <= rad_check) {
				enemy_count++;
			} else {
				break;
			}
		}
		
		cap = enemy_count;
		cap = 1;
		
		if (int(defending_ships.size()) > cap) {
			std::sort(defending_ships.begin(), defending_ships.end(), [p](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(p->location) < b->location.get_distance_to(p->location);});
		}
		
		while (int(defending_ships.size()) > cap) {
			hlt::Ship* s2 = defending_ships.back();
			defending_ships.pop_back();
			if (num_players == 2) {
                s2->behavior = default_behavior_2p;
            } else {
                s2->behavior = default_behavior_4p;
				s2->behavior = default_behavior_2p;
            }
            s2->command = -1;
            s2->behavior->find_targets(s2);
		}		
	}

	/*
	// we don't need to have EVERY ship defending. release newer id ships for defense.
	for (hlt::Ship* s : my_docked_ships) {
		if (int(s->targeted.size()) > cap) {
			// sort by id.
			// std::sort(s->targeted.begin(), s->targeted.end(), [](hlt::Ship* a, hlt::Ship* b) { return a->entity_id < b->entity_id; });
            // sort by distance, release furthest ships first.
            std::sort(s->targeted.begin(), s->targeted.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
		}

		while (int(s->targeted.size()) > cap) {
			hlt::Ship* s2 = s->targeted.back();
			s->targeted.pop_back();
            if (num_players == 2) {
                s2->behavior = default_behavior_2p;
            } else {
                s2->behavior = default_behavior_4p;
            }
            s2->command = -1;
            s2->behavior->find_targets(s2);
		}
	}
	*/
}

void move_to_closest_corner(hlt::Ship* s) {

    hlt::Log::log("Moving ship id: " + to_string(s->entity_id) + " to a corner", 10);
	std::vector<std::pair<hlt::Location, double>> corners;
	hlt::Location NW, NE, SW, SE;
	NW.pos_x = 1, NW.pos_y = 1;
	NE.pos_x = game_map->map_width - 2, NE.pos_y = 1;
	SW.pos_x = 1, SW.pos_y = game_map->map_height - 2;
	NW.pos_x = game_map->map_width - 2, NW.pos_y = game_map->map_height - 2;

	corners.push_back(std::make_pair(NW, s->location.get_distance_to(NW)));
	corners.push_back(std::make_pair(NE, s->location.get_distance_to(NE)));
	corners.push_back(std::make_pair(SW, s->location.get_distance_to(SW)));
	corners.push_back(std::make_pair(SE, s->location.get_distance_to(SE)));

	std::sort(corners.begin(), corners.end(), [](std::pair<hlt::Location, double> a, std::pair<hlt::Location, double> b) { return std::get<1>(a) < std::get<1>(b); });
	s->entity_target = NULL;
	s->target = std::get<0>(corners[0]);
	s->command = 1;
    s->nav = default_navigation;

}

bool ship_in_corner() {

	hlt::Location NW, NE, SW, SE;
	NW.pos_x = 1, NW.pos_y = 1;
	NE.pos_x = game_map->map_width - 2, NE.pos_y = 1;
	SW.pos_x = 1, SW.pos_y = game_map->map_height - 2;
	NW.pos_x = game_map->map_width - 2, NW.pos_y = game_map->map_height - 2;

	std::vector<hlt::Location> corners;
	corners.push_back(NW);
	corners.push_back(SW);
	corners.push_back(NE);
	corners.push_back(SE);

	// goes through all ships and identify if we have one in the corner.
	for (hlt::Ship* s : my_ships) {
		for (hlt::Location& c : corners) {
			if (s->location.get_distance_to(c) <= 3 || s->target.get_distance_to(c) <= 3) {
				s->command = -1;
                hlt::Log::log("Ship found in corner", 10);
				return true;
			}
		}
	}
    hlt::Log::log("No ship found in corner", 10);
	return false;
}

double get_distance_to_closest_corner(hlt::Ship* s) {
	std::vector<std::pair<hlt::Location, double>> corners;
	hlt::Location NW, NE, SW, SE;
	NW.pos_x = 1, NW.pos_y = 1;
	NE.pos_x = game_map->map_width - 2, NE.pos_y = 1;
	SW.pos_x = 1, SW.pos_y = game_map->map_height - 2;
	NW.pos_x = game_map->map_width - 2, NW.pos_y = game_map->map_height - 2;

	corners.push_back(std::make_pair(NW, s->location.get_distance_to(NW)));
	corners.push_back(std::make_pair(NE, s->location.get_distance_to(NE)));
	corners.push_back(std::make_pair(SW, s->location.get_distance_to(SW)));
	corners.push_back(std::make_pair(SE, s->location.get_distance_to(SE)));

	std::sort(corners.begin(), corners.end(), [](std::pair<hlt::Location, double> a, std::pair<hlt::Location, double> b) { return std::get<1>(a) < std::get<1>(b); });

	return std::get<1>(corners[0]);
}

void hide_ship_in_corner() {
    hlt::Log::log("Attempting to hide a ship in the corner.", 10);
	if (ship_in_corner() || my_undocked_ships.size() == 0) { return ; }

	// find the closest ship to a corner and move them to it.
	std::sort(my_undocked_ships.begin(), my_undocked_ships.end(), [](hlt::Ship* a, hlt::Ship* b) { return get_distance_to_closest_corner(a) < get_distance_to_closest_corner(b); });

	move_to_closest_corner(my_undocked_ships[0]);
}

bool avoid_enemy(hlt::Ship* s) {
    // we use this to active retreat.

	if (s->docking_status == hlt::ShipDockingStatus::Undocked) {
		s->target = s->location;    
		if (s->rand2 < 1) {
			s->nav = retreat_from_all;
		} else {
			s->nav = coward;
		}	
		s->command = 1;
	} else if (s->docking_status == hlt::ShipDockingStatus::Docked) {
		s->command = 3;
	}
    return true;
}

bool reassign_ships_over_attack_capacity(int cap) {
    // We don't need to send ships over the docking capacity of a planet (what about planets near the combat border??)
    // Take ships over the border and reassign them to other planets. Note, calling this function WILL modify existing planet vectors.

    hlt::Log::log("Checking to see if ships need to be reassigned to other ships to attack.", 9);

    // For some reason erase/remove_if doesn't modify the vector properly. so we'll just loop through it ourself.
    hlt::Log::log("size of undocked enemy ships: " + to_string(enemy_undocked_ships.size()), 10);
    hlt::Log::log("size of docked enemy ships: " + to_string(enemy_docked_ships.size()), 10);
    std::vector<hlt::Ship*> new_undocked_enemy_ships;
    std::vector<hlt::Ship*> new_docked_enemy_ships;
    std::vector<hlt::Ship*> new_enemy_ships;
    std::vector<hlt::Ship*> excess_ships;

    for (hlt::Ship* s : enemy_undocked_ships) {
        if (int(s->targeted.size()) <= cap) {
            new_undocked_enemy_ships.push_back(s);
			new_enemy_ships.push_back(s);
        } else {
            excess_ships.push_back(s);
        }
    }
	
    for (hlt::Ship* s : enemy_docked_ships) {
        if (int(s->targeted.size()) <= cap) {
            new_docked_enemy_ships.push_back(s);
			new_enemy_ships.push_back(s);
        } else {
            // excess_ships.push_back(s);
			new_enemy_ships.push_back(s);
        }
    }
	
    // for (hlt::Ship* s : enemy_ships) {
    //     if (int(s->targeted.size()) <= cap) {
    //         new_enemy_ships.push_back(s);
    //     }
    // }

    hlt::Log::log("size of new_undocked_enemy_ships: " + to_string(new_undocked_enemy_ships.size()), 10);
    hlt::Log::log("size of new_docked_enemy_ships: " + to_string(new_docked_enemy_ships.size()), 10);
    if (enemy_ships.size() == new_enemy_ships.size()) {
        hlt::Log::log("No ships assigned over capacity.", 9);
        return false;
    }
	enemy_undocked_ships.clear();
	for (auto a : new_undocked_enemy_ships) {
		enemy_undocked_ships.push_back(a);
	}
	// enemy_docked_ships.clear();
	// for (auto a : new_docked_enemy_ships) {
	// 	enemy_docked_ships.push_back(a);
	// }
	enemy_ships.clear();
	for (auto a : new_enemy_ships) {
		enemy_ships.push_back(a);
	}

	enemy_attacking_ships.clear();
	for (hlt::Ship* s : enemy_ships) {
        for (hlt::Ship* s2 : my_docked_ships) {
            if (s->location.get_distance_to(s2->location) <= hlt::constants::MAX_SPEED * 4) {
                enemy_attacking_ships.push_back(s);
                break;
            }
        }
    }

    for (hlt::Ship* s : excess_ships) {
        int num_extra = s->targeted.size() - cap;
        hlt::Log::log("Enemy ship: " + to_string(s->entity_id) + " has " + to_string(num_extra) + " ships to reassign.");
        std::sort(s->targeted.begin(), s->targeted.end(), [s](hlt::Ship* a, hlt::Ship* b) { return s->location.get_distance_to(a->location) > s->location.get_distance_to(b->location); });

        for (int i = 0; i < num_extra; ++i) {
            hlt::Ship* s2 = s->targeted[i];
            if (num_players == 2) {
                s2->behavior = default_behavior_2p;
            } else {
                s2->behavior = default_behavior_4p;
				s2->behavior = default_behavior_2p;
            }
            s2->command = -1;
            s2->nav = NULL;
            s2->behavior->find_targets(s2);
        }
    }
    return true;
}

void update_ship_attacks() {
	// goes through and updates all ships values to see how many ships are projected to be attacking or attacked.
	for (hlt::Ship* s : all_ships) {		
		s->update_ships_in_attack_range();
	}
}

void send_ally_signal() {
	// Sends ally signal.

	// func (g *Game) DoBeeDance(command Command) {
    // if command.Ship.GetId() == g.pid3+int(math.Mod(float64(g.pid), 3.0)) {
    //     command.Speed = 6
    // }
    // 	command.Angle = int(math.Floor(float64(command.Angle)/9.0))*9 + ModInt(g.pid+command.Ship.GetId(), 9)
	// }

	hlt::Log::log("Sending ally signal");

	int speed_6 = player_id * 3 + player_id % 3;

	hlt::Log::log("Speed6 ship entity id: " + to_string(speed_6));

	for (hlt::Ship* s : my_ships) {
		if (s->entity_id == speed_6) {
			s->speed = 6;
			hlt::Log::log("Setting " + to_string(s->entity_id) + " to speed 6");
		}
		int angle_signal = (s->entity_id + player_id) % 9;
		hlt::Log::log("Setting " + to_string(s->entity_id) + " to angle_signal: " + to_string(angle_signal));

		bool ok = false;

		while (!ok) {
			ok = true;
			while (s->angle % 9 != angle_signal) {
				s->angle += 1;
				if (s->angle >= 360) { s->angle = 0; }			
			}
			hlt::Log::log("testing collision at angle: " + to_string(s->angle));
			s->update();
			for (auto f : my_ships) {
				if (collision_check(s, f)) {
					ok = false;
					break;
				}			
			}
			if (!ok) { s->angle++; }
		}
	}
}

bool check_allied() {

	for (auto q : enemy_ships) {
		if (q->owner_id != ally_id) {
			return true;
		}
	}

	return false;
}

void predict_enemy_moves() {
	for (hlt::Ship* s : enemy_ships) {
		if (s->speed > 0) {
			s->speed = int(s->speed / 2);
		}
	}
}

void predict_enemy_moves_2() {

	// try to improve upon basic enemy prediction slightly.

	// for now, set our ships projected vector to be the same as the prior turn.

	for (hlt::Ship* s : my_ships) {
		std::unordered_map<int, hlt::Ship>::iterator it = last_turn_ships.find(s->entity_id);
		if (it != last_turn_ships.end()) {
			hlt::Ship i = it->second;
			// If this ship's weapon cooldown is currently 1 (what if ships try to retreat after firing?)
			double speed = sqrt((s->location.pos_x - i.location.pos_x) * (s->location.pos_x - i.location.pos_x) + (s->location.pos_y - i.location.pos_y) * (s->location.pos_y - i.location.pos_y));
			int angle = s->location.angle_to(i.location);
			s->speed = speed;
			s->angle = angle;
			s->update();						
		} else {
			// just to make sure projected location is updated with current position.
			s->update();
		}
	}

	for (int i = 0; i < num_players; ++i) {
		if (i == player_id) { continue; }

		// recreate filtered vectors from the perspective of the enemy.
		create_filtered_vectors(i);

		// do very very simple targeting.

		// assume that we target the nearest enemy.
		for (hlt::Ship* s : my_ships) {
			if (s->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
			
			// let's see if this works...
			/*
			if (num_players == 2) {
				s->behavior = default_behavior_2p;
			} else {
				s->behavior = default_behavior_4p;
			}
			s->behavior->find_targets(s);
			*/

			if (s->nearby_enemy_ships.size() > 0) {
				s->entity_target = s->nearby_enemy_ships[0];
				s->target = s->nearby_enemy_ships[0]->location;
				s->nav = dogfight;

				hlt::navigation::determine_moves(true);
				hlt::navigation::determine_moves(false);
			}
		
		}

	}	

	// restore our filtered vectors
	create_filtered_vectors(player_id);

	// reset our own units speed/angles
	for (hlt::Ship* s : my_ships) {
		s->speed = 0;
		s->angle = 0;
		s->update();
	}
}

bool determine_4p_survival_mode() {

	// determines if we should go into full retreat & survival mode.

	if (rush_mode) {
		if (my_docked_ships.size() > 0) { return false;}
		// we're rushing, we should retreat if we're going to lose this battle.

		// we would run if we're outnumbered in ships, or we have equal ships but less health.

		int target_enemy_ship_count = 0;
		double target_enemy_health_count = 0;

		for (auto s : enemy_ships) {
			if (s->owner_id != rush_id) { continue; }
			if (s->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
			++target_enemy_ship_count;
			target_enemy_health_count += s->health;
		}

		double my_health = 0;
		for (auto s : my_ships) {
			my_health += s->health;
		}

		hlt::Log::log("target_enemy_ship_count: " + to_string(target_enemy_ship_count));
		hlt::Log::log("target_enemy_health_count: " + to_string(target_enemy_health_count));
		hlt::Log::log("my_health: " + to_string(my_health));

		if (target_enemy_ship_count > my_ships.size()) { return true; }
		if (target_enemy_health_count > my_health && target_enemy_ship_count == my_ships.size()) { return true; }

		return false;
	}

	// otherwise, we should retreat when the following conditions are true.

	// a bit inefficient but it'll work...
	bool we_have_docked = false;
	for (auto s : my_ships) {
		if (s->p_ship_num > 3) {
			we_have_docked = true;
			break;
		}
	}

	// if we haven't docked yet, then we shouldn't need to retreat, else rush should probably have it covered?
	if (!we_have_docked) {
		// unless we were pre-emptively attacked.
		if (my_ships.size() < 3 && rush_id == -1) {
			return true;
		}

		return false;
	}

	// if we have docked and we have no docked ships left, then let's run.
	if (my_docked_ships.size() == 0) {
		return true;
	}

	// alternative, if we're down to 2 planets or fewer, and we're massively outnumbered, then retreat.
	if (my_planets.size() <= 2) {
		// check that we're completely outnumbered.

		// if there is an enemy that has at least 50% of the planets, then we're going to lose.
		std::array<int, 4> enemy_planets_owned = {0, 0, 0, 0};

		for (auto p : enemy_planets) {
			++enemy_planets_owned[p->owner_id];
		}

		int max_enemy = 0;
		for (int a : enemy_planets_owned) {
			if (a > max_enemy) { 
				max_enemy = a;
			}
		}

		if (2 * max_enemy >= all_planets.size()) {
			return true;
		}
	}

	return false;
}