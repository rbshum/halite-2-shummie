#pragma once

#include <cassert>

#include "../hlt/globals.h"
#include "../hlt/hlt.hpp"
#include "../hlt/planet.hpp"
#include "../hlt/log.hpp"
#include "filters.h"

void update_planet_data();
void create_filtered_vectors(int player);
void create_filtered_vectors_ally(int player);
void score_all_planets();
double score_planet(hlt::Planet* p);
double score_planet_settle(hlt::Planet* p);
void assign_p_ship_num();
void simulate_start_of_turn();
void populate_nearby_ships();
std::vector<hlt::Ship*> get_all_enemy_ships_in_weapon_range(hlt::Ship* s);
void save_last_game_ships();
void process_last_turn_ship_data();
void check_ally_signal();

