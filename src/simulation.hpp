#pragma once

#include "../hlt/map.hpp"
#include "../hlt/globals.h"
#include "../hlt/types.hpp"
#include "../hlt/hlt.hpp"
#include "../hlt/planet.hpp"
#include "../hlt/ship.hpp"
#include "simulation_event.hpp"
#include <vector>
#include <unordered_map>
#include <unordered_set>

constexpr auto EVENT_TIME_PRECISION = 10000;

void process_docking(hlt::Map* game);
hlt::Map simulate_turn(hlt::Map* game);
