#pragma once

#include <cassert>
#include <array>

#include "../hlt/globals.h"
#include "../hlt/hlt.hpp"
#include "../hlt/log.hpp"
#include "filters.h"
#include "base_functions.h"
#include "behavior.h"
#include "../hlt/navigation.hpp"

void move_or_dock_to_planet(hlt::Ship* s, hlt::Planet* p, double buffer);

void early_game_match_targets();

bool reassign_ships_over_docking_capacity();
void set_rush_mode();
void make_formation();
void attempt_formations();

std::vector<hlt::Ship*> get_all_friendly_undocked_ships_in_range_not_targeting_friendly(hlt::Ship* s, double distance);

void reassign_ships_over_defense_capacity(int cap);
void move_to_closest_corner(hlt::Ship* s);
void hide_ship_in_corner();
double get_distance_to_closest_corner(hlt::Ship* s);
bool ship_in_corner();
bool avoid_enemy(hlt::Ship* s);
bool reassign_ships_over_attack_capacity(int cap);
void update_ship_attacks();
void send_ally_signal();
bool check_allied();
void predict_enemy_moves();
bool determine_4p_survival_mode();