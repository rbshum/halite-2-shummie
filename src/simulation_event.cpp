#include "simulation_event.hpp"

bool test_aabb_circle(int rect_x, int rect_y, int rect_w, int rect_h, const hlt::Location& circ_center, double radius) {
    const auto x_half_rect = rect_w / 2.0;
    const auto y_half_rect = rect_h / 2.0;
    const auto x_dist = std::abs(circ_center.pos_x - rect_x - x_half_rect);
    const auto y_dist = std::abs(circ_center.pos_y - rect_y - y_half_rect);

    if (x_dist > x_half_rect + radius) return false;
    if (y_dist > y_half_rect + radius) return false;

    if (x_dist <= x_half_rect) return true;
    if (y_dist <= y_half_rect) return true;

    const auto dx = x_dist - x_half_rect;
    const auto dy = y_dist - y_half_rect;

    return std::pow(dx, 2) + std::pow(dy, 2) <= std::pow(radius, 2);
}

/*
void Collision_Map::rebuild(const hlt::Map& game, const std::function<double(const hlt::Ship&)> radius_func) {
    int player = 0;
    for (const auto& player_ships : game.ships) {
        for (const auto& ship : player_ships.second) {
            const auto& location = ship.location;
            const auto id = ship.entity_id;
            add(location, radius_func(ship), id);
            player++;
        }
    }
}
*/