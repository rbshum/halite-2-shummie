#pragma once

#include <iostream>
#include <functional>
#include <unordered_set>
#include <vector>

#include "../hlt/entity.hpp"
#include "../hlt/hlt.hpp"

/*
class Collision_Map {
public:
    constexpr static double CELL_SIZE = 32;

    std::vector<std::vector<std::vector<int>>> cells;

    int w, h;

    Collision_Map(const hlt::Map& game, const std::function<double(const hlt::Ship&)> radius_func);

    void rebuild(const hlt::Map& game, const std::function<double(const hlt::Ship&)> radius_func);
    void test(const hlt::Location& location, double radius, std::vector<int>& potential_collisions);
    void add(const hlt::Location& location, double radius, int id);

};
*/

class Simulation_Event{

public:
    int type; // 1 = Attack, 2 = Collision, 3 = Desertion (out of bounds)
    hlt::Entity* id1;
    hlt::Entity* id2;
    double time;

    bool operator==(const Simulation_Event &rhs) const {
        return type == rhs.type && ((id1 == rhs.id1 && id2 == rhs.id2) || (id1 == rhs.id2 && id2 == rhs.id1));
    }

    bool operator!=(const Simulation_Event &rhs) const {
        return !(rhs == *this);
    }

    friend std::ostream& operator<<(std::ostream &os, const Simulation_Event &event) {
        os << "Simulation_Event(type: " << event.type << " id1: " << event.id1->entity_id << " id2: " << event.id2->entity_id << " time: " << event.time << ")";
        return os;
    }
};

namespace std {
    template<> struct hash<Simulation_Event> {
        std::size_t operator()(const Simulation_Event& ev) const {
            return std::hash<int>{}(ev.id1->entity_id) ^ std::hash<int>{}(ev.id2->entity_id) ^ static_cast<size_t>(ev.type);
        }
    };
}