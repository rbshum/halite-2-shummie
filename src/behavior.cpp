#include "behavior.h"

double Default_Navigation::score_move(hlt::Ship* s, bool initial) {
	// If we're far enough away, try to move faster so that we reach the target sooner.
	bool faster = false;
	if (s->entity_target != NULL) {
		faster = true;
		if (s->entity_target->location.get_distance_to(s->target) <= hlt::constants::MAX_SPEED) {
			faster = false;
		}
	}

    int sort_size = std::min(5, int(enemy_undocked_ships.size()));
    std::partial_sort(enemy_undocked_ships.begin(), enemy_undocked_ships.begin() + sort_size, enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->projected_location.get_distance_to(s->projected_location) < b->projected_location.get_distance_to(s->projected_location); });

    double move_score;
    
	if (faster) {
		move_score = -s->projected_location.get_distance_to(s->entity_target->location) + s->speed * 0.8;
	} else {
        double dist = s->projected_location.get_distance_to(s->target);
        if (dist <= hlt::constants::SHIP_RADIUS * 2 + 0.05) {
            move_score = -dist - 999;
        } else {
            move_score = -dist;
        }
	}

    for (int i = 0; i < sort_size; ++i) {
        hlt::Ship* e = enemy_undocked_ships[i];
        double dist = s->projected_location.get_distance_to(e->projected_location);

        move_score += dist * 0.07;

    }

    return move_score;

}

double Coward::score_move(hlt::Ship* s, bool initial) {
	// Attempts to retreat from all undocked enemy ships.

	// std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    // int sort_size = std::min(10, int(s->nearby_enemy_ships.size()));
    int sort_size = std::min(10, int(enemy_undocked_ships.size()));
    double move_score = 0;

   	std::partial_sort(enemy_undocked_ships.begin(), enemy_undocked_ships.begin() + sort_size, enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location);});

	double attack_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
    double undocked_dist = hlt::constants::MAX_SPEED * 3.5 + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
	double too_far = hlt::constants::SHIP_RADIUS * 2 + hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED * 6;
	
    for (int i = 0; i < sort_size; ++i) {        
        hlt::Ship* e = enemy_undocked_ships[i];
        double dist = s->projected_location.get_distance_to(e->location);
				
		if (dist <= undocked_dist) {
			move_score -= 1000;
			if (dist <= attack_dist) {
				move_score -= 10000;
			}
			move_score += dist;
		} else if (dist >= too_far) {
			move_score -= 200;
			move_score -= dist;
		} else {
			move_score -= dist;
		}
		
		

        if (dist <= undocked_dist && e->docking_status == hlt::ShipDockingStatus::Undocked) {
            move_score -= 1000;
        }

        move_score += dist;
    }

	return move_score;	
}

double Navigate_To_Planet::score_move(hlt::Ship* s, bool initial) {
	// If we're far enough away, try to move faster so that we reach the target sooner.
	bool faster = false;
	if (s->entity_target != NULL) {
		faster = true;
		if (s->entity_target->location.get_distance_to(s->target) <= hlt::constants::MAX_SPEED + hlt::constants::DOCK_RADIUS + s->entity_target->radius) {
			faster = false;
		}
	}

    int sort_size = std::min(4, int(s->nearby_friendly_ships.size()));
    
    double move_score = 0;

	if (faster) {
		move_score = -s->projected_location.get_distance_to(s->entity_target->location) + s->speed * 0.8;
	} else {
        double dist = s->projected_location.get_distance_to(s->target);

        if (dist <= hlt::constants::DOCK_RADIUS + s->entity_target->radius - s->radius) {
            move_score += 9000 - dist;
        } else {
            move_score = -dist;
        }
	}

    // try to go to a place that's away from the enemy...
    for (hlt::Ship* q : s->nearby_enemy_ships) {
        double dist = s->projected_location.get_distance_to(q->location);
        move_score += 0.05 * dist;
    }

    if (!initial) {
        for (int i = 0; i < sort_size; ++i) {
            hlt::Ship* e = s->nearby_friendly_ships[i];
            if (e->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
            double dist = s->projected_location.get_distance_to(e->projected_location);
            move_score += dist * 0.07;
        }
    }

    return move_score;
}

double Navigate_To_Planet_4p_Rush::score_move(hlt::Ship* s, bool initial) {
	// If we're far enough away, try to move faster so that we reach the target sooner.
	bool faster = false;
	if (s->entity_target != NULL) {
		faster = true;
		if (s->entity_target->location.get_distance_to(s->target) <= hlt::constants::MAX_SPEED + hlt::constants::DOCK_RADIUS + s->entity_target->radius) {
			faster = false;
		}
	}

    int sort_size = std::min(4, int(s->nearby_friendly_ships.size()));
    
    double move_score = 0;

    double dist = s->projected_location.get_distance_to(s->target);

    if (dist <= hlt::constants::DOCK_RADIUS + s->entity_target->radius - s->radius) {
        move_score += 9000 - dist;
    } else {
        move_score = -dist;
    }

    for (hlt::Ship* q : s->nearby_enemy_ships) {
        double dist = s->projected_location.get_distance_to(q->location);
        move_score += 0.05 * dist;
    }

    if (!initial) {
        for (int i = 0; i < sort_size; ++i) {
            hlt::Ship* e = s->nearby_friendly_ships[i];
            if (e->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
            double dist = s->projected_location.get_distance_to(e->projected_location);
            move_score -= dist * 0.07;
        }
    }

    return move_score;
}


double Retreat_From_All::score_move(hlt::Ship* s, bool initial) {
	// Attempts to retreat from all undocked enemy ships.

	// std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    // int sort_size = std::min(10, int(s->nearby_enemy_ships.size()));
    int sort_size = std::min(10, int(enemy_ships.size()));
    double move_score = 0;

    /*
    if (sort_size > 0) {
        move_score = s->projected_location.get_distance_to(s->nearby_enemy_ships[0]->location);
    } else {
        sort_size = std::min(15, int(enemy_ships.size()));
        std::partial_sort(enemy_ships.begin(), enemy_ships.begin() + sort_size, enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location);});
        move_score = s->projected_location.get_distance_to(enemy_ships[0]->location);
    }
	*/
	
	std::partial_sort(enemy_ships.begin(), enemy_ships.begin() + sort_size, enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location);});
        

    double undocked_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;

    for (int i = 0; i < sort_size; ++i) {        
        hlt::Ship* e = enemy_ships[i];
        double dist = s->projected_location.get_distance_to(e->location);

        if (dist <= undocked_dist && e->docking_status == hlt::ShipDockingStatus::Undocked) {
            move_score -= 1000;
        }

        move_score += dist;
    }

	return move_score;
}

double Retreat_From_All_Group_Friendly::score_move(hlt::Ship* s, bool initial) {
	// Attempts to retreat from all undocked enemy ships.
	// if we are in range of docked enemy ships, give a slight preference towards being in range of them.

	// std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    int sort_size = std::min(10, int(s->nearby_enemy_ships.size()));
    double move_score = 0;

    if (sort_size > 0) {
        move_score = s->projected_location.get_distance_to(s->nearby_enemy_ships[0]->location);
    } else {
        sort_size = std::min(10, int(enemy_ships.size()));
        std::partial_sort(enemy_ships.begin(), enemy_ships.begin() + sort_size, enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location);});
        move_score = s->projected_location.get_distance_to(enemy_ships[0]->location);
    }
	

    double undocked_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;


    for (hlt::Ship* e : s->nearby_enemy_ships) {

        double dist = s->projected_location.get_distance_to(e->location);
        move_score += dist;

    }

    sort_size = std::min(6, int(my_ships.size()));
    std::partial_sort(my_ships.begin(), my_ships.begin() + sort_size, my_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return s->projected_location.get_distance_to(a->location) < s->projected_location.get_distance_to(b->location);});
    
    for (int i = 1; i < sort_size; ++i) {
        if (my_ships[i]->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
        double d_t = s->projected_location.get_distance_to(my_ships[i]->location);
        move_score -= d_t;
    }
	return move_score;
}

double Attack_Docked_Ship::score_move(hlt::Ship* s, bool initial) {

    if (s->nearby_enemy_ships.size() > 0) {
        int enemy_count = 0;
        int friend_count = 0;
        double friend_dist = (hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS) * 0.4;
        double enemy_dist = hlt::constants::MAX_SPEED * 0.4 + hlt::constants::WEAPON_RADIUS;

        for (hlt::Ship* q : s->nearby_enemy_ships) {
            if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
                if (q->projected_location.get_distance_to(s->projected_location) <= enemy_dist) {
                    ++enemy_count;
                }
            }
        }
        for (hlt::Ship* q : s->nearby_friendly_ships) {
            if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
                if (q->projected_location.get_distance_to(s->projected_location) <= friend_dist) {
                    ++friend_count;
                }
            }
        }
        if (enemy_count > 4) { enemy_count = 4; }
        if (friend_count > 5) { friend_count = 5; }
        if (enemy_count > 0 && enemy_count > friend_count || (enemy_count == 1 && friend_count == 1)) {
            return retreat_from_all_group_friendly->score_move(s, initial);
        }
    }
	// Attempts to attack a docked ship.
	// will try to avoid all other ships if possible.
	// less of a penalty for docked ships than undocked ships.
	// std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    double move_score = 0;
    int sort_size;

    if (!initial) {
        sort_size = std::min(4, int(s->nearby_friendly_ships.size()));
        for (int i = 1; i < sort_size; ++i) {
            if (s->nearby_friendly_ships[i]->docking_status == hlt::ShipDockingStatus::Undocked) {
                double d_t = s->projected_location.get_distance_to(s->nearby_friendly_ships[i]->projected_location);
                if (d_t < hlt::constants::SHIP_RADIUS * 2 + 0.05) {
                    move_score -= 100000;
                } else if (d_t <= hlt::constants::MAX_SPEED * 2) {
                    move_score -= d_t * 0.2;
                }
            }
        }

        int friendly_count = 0;
        for (int i = 1; i < sort_size; ++i) {
            if (s->nearby_friendly_ships[i]->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
            double d_t = s->projected_location.get_distance_to(s->nearby_friendly_ships[i]->projected_location);
            if (d_t < hlt::constants::SHIP_RADIUS * 2 + 0.05) {
                move_score -= 100000;
            } else {
                move_score -= d_t * 0.4;
                if (d_t <= 2) {
                    friendly_count++;
                }
            }

        }
        if (friendly_count > s->nearby_enemy_ships.size()) {
            friendly_count = s->nearby_enemy_ships.size();
        }
        move_score += friendly_count * 10000;
    }

    sort_size = std::min(10, int(s->nearby_enemy_ships.size()));    

	move_score -= s->projected_location.get_distance_to(s->entity_target->location);

    double undocked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 + 0;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 0.1;


	for (hlt::Ship* e : s->nearby_enemy_ships) {

        double dist = s->projected_location.get_distance_to(e->projected_location);
        if (dist > undocked_dist) {
            break;
        }

        if (e->docking_status == hlt::ShipDockingStatus::Undocked) {
            move_score += dist;
            move_score += 1000;  // do we want to keep this??
        } else {

            if (dist <= hlt::constants::SHIP_RADIUS * 2 + 0.05) {
                move_score -= 10000;
            } else {
                if (dist < docked_dist) {
                    if (e == s->entity_target) {
                        move_score -= -1000 - 2 * dist;
                    } else {
                        // move_score += 50 - dist;
                    }
                }
            }
        }
	}
	return move_score;
}


double Attack_Docked_Ship_Avoid_All::score_move(hlt::Ship* s, bool initial) {
	// Attempts to attack a docked ship.
	// will try to avoid all other ships if possible.
	// less of a penalty for docked ships than undocked ships.
	// std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    int sort_size = std::min(10, int(s->nearby_enemy_ships.size()));
    
	double move_score = 0;
    move_score += -s->projected_location.get_distance_to(s->entity_target->location);

    double undocked_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;

    int out_of_range_ships = 0;
    int docked_ships_in_range = 0;

    for (hlt::Ship* e : s->nearby_enemy_ships) {

        double dist = s->projected_location.get_distance_to(e->location);
        if (dist > undocked_dist) {
            if (dist > s->projected_location.get_distance_to(s->entity_target->location)) {
                break;
            }
            /*
            if (e->docking_status == hlt::ShipDockingStatus::Undocked) {
                out_of_range_ships ++;
                move_score += .1 * dist;
                if (out_of_range_ships > 2) {
                    break;
                }
            }
            */
        } else {

            if (e->docking_status == hlt::ShipDockingStatus::Undocked) {
                move_score += -100000 + dist;
            } else {                
                if (dist <= hlt::constants::SHIP_RADIUS * 2) {
                    move_score -= 10000;
                } else {
                    if (dist <= docked_dist) {
                        docked_ships_in_range++;
                        if (e == s->entity_target) {
                            move_score += 1000 + 2 * dist;
                        } else {
                            move_score += 200 + dist;
                        }
                    }
                }
            }
        }        
    }

    if (docked_ships_in_range == 1) { 
        move_score += 10000;
    }

    /*
    if (!initial) {
        sort_size = std::min(4, int(s->nearby_friendly_ships.size()));
        for (int i = 1; i < sort_size; ++i) {
            if (s->nearby_friendly_ships[i]->docking_status == hlt::ShipDockingStatus::Undocked) {
                double d_t = s->projected_location.get_distance_to(s->nearby_friendly_ships[i]->projected_location);
                if (d_t < hlt::constants::SHIP_RADIUS * 2 + 0.05) {
                    move_score -= 100000;
                } else {
                    move_score += d_t * 0.12;
                }
            }
        }
    }
    */

	return move_score;
}

double Dogfight::score_move(hlt::Ship* s, bool initial) {
    // will attempt to hover just outside of range

    double move_score = 0;

    // if no enemies are nearby...
    if (s->nearby_enemy_ships.size() == 0) {
        // just move towards our target.        
        move_score += -s->projected_location.get_distance_to(s->entity_target->location);
        return move_score;
    }

    // Enemies are nearby... stay just outside of their potential attack radius.

    double attack_radius = hlt::constants::WEAPON_RADIUS + 2 * hlt::constants::SHIP_RADIUS + hlt::constants::MAX_SPEED;
    double docked_radius = hlt::constants::WEAPON_RADIUS + 2 * hlt::constants::SHIP_RADIUS;

    // do we have numerical advantage??

    // start with a VERY conservative estimate.
    double nearby_friendly = 0;
    double nearby_enemy = 0;
	double all_nearby_enemy = 0;

    bool docked_in_range = false;

    double nearby_dist;
    nearby_dist = (hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + 2 * hlt::constants::SHIP_RADIUS) * 1;
    if (rush_mode) {
        
    } else {
        nearby_dist *= 1;
    }
    for (hlt::Ship* q : s->nearby_enemy_ships) {
        double dist = s->projected_location.get_distance_to(q->location);
		if (dist <= attack_radius * 1 && q->docking_status == hlt::ShipDockingStatus::Undocked) {
			all_nearby_enemy++;
		}
		
        if (q->docking_status == hlt::ShipDockingStatus::Undocked && dist <= nearby_dist && q->weapon_cooldown == 0) {
            nearby_enemy++;			
        }

        if (q->docking_status == hlt::ShipDockingStatus::Undocked && dist <= nearby_dist && q->weapon_cooldown == 1) {
            nearby_enemy += 0.5;
        }
        
        if (q->docking_status != hlt::ShipDockingStatus::Undocked && dist <= nearby_dist) {
            // nearby_enemy += 0.25;
        }

        if (q->docking_status != hlt::ShipDockingStatus::Undocked && dist <= docked_radius) {
            docked_in_range = true;
        }
    }

    if (initial) {
        nearby_dist = 5;    
    } else {
        if (num_players == 2) {
            nearby_dist = 8;
        } else {
            nearby_dist = 8;
        }
    }
    

    for (hlt::Ship* q : s->nearby_friendly_ships) {
        double dist;
        if (initial) {
            dist = s->location.get_distance_to(q->location);
        } else {
            dist = s->projected_location.get_distance_to(q->projected_location);
        }
        
        if (q->docking_status == hlt::ShipDockingStatus::Undocked && dist <= nearby_dist && q->weapon_cooldown == 0) {
            nearby_friendly++;
        }
        
        if (q->docking_status == hlt::ShipDockingStatus::Undocked && dist <= nearby_dist && q->weapon_cooldown == 1) {
            nearby_friendly += 0.5;
        }

        if (q->docking_status != hlt::ShipDockingStatus::Undocked && dist <= nearby_dist) {
            nearby_friendly += 1;
        }
    }

    double aggression = 1;
    if (nearby_friendly >= nearby_enemy || (docked_in_range && nearby_friendly >= 4)) {
        aggression = -1;
    }

    
    if (rush_mode) {
        int might_attack_count = 0;

        for (auto q : s->nearby_enemy_ships) {
            if (might_attack(s, q)) {
                if (s->health < q->health && q->docking_status == hlt::ShipDockingStatus::Undocked) {
                    might_attack_count += 2;
                } else {
                    might_attack_count++;
                }
            }
            if (might_attack_count == 1) {
                move_score += 1500;
            } else {
                move_score -= might_attack_count * 1500;
            }
        }
    }

    if (aggression == 1 && nearby_friendly == 0) {
        // return retreat_from_all_group_friendly->score_move(s, initial);
    }

    /*
    if (aggression == 1) {
        move_score += 1.75 * s->projected_location.get_distance_to(s->entity_target->location);
    } else {
        move_score += -1.75 * s->projected_location.get_distance_to(s->entity_target->location);
    }
    */

    move_score += -2.8 * s->projected_location.get_distance_to(s->entity_target->location);

    // move_score += -1.5 * s->projected_location.get_distance_to(s->target);
    if (aggression == -1) {
        // move_score += 0.75 * s->projected_location.get_distance_to(s->location);
    }

    // if (initial && !rush_mode) {
    if (initial) {
        aggression = -1;
    }

	if (num_players == 4 && my_ships.size() < enemy_ships.size() && s->nearby_enemy_ships.size() > 1) {
		// retreat if there are multiple players nearby.
		bool multiple_enemies = false;
		int e_id = s->nearby_enemy_ships[0]->owner_id;
		for (auto q : s->nearby_enemy_ships) {
			if (q->location.get_distance_to(s->projected_location) > attack_radius) { continue; }
			if (q->owner_id != e_id) {
				multiple_enemies = true;
				break;
			}
		}
		
		if (multiple_enemies) {
			// for now, just retreat.
			// for now, penalize
            move_score -= 10000;
		}
	}
	
	// move around planets?
	
	for (auto p : all_planets) {
		double dist = p->location.get_distance_to(s->projected_location);
		if (dist <= p->radius + 3) {
			move_score += dist - 10;
		}
	}
	
    // if we're being aggressive...
    // if (aggression == -1 && !rush_mode && s->weapon_cooldown == 0) {
    // if (aggression == -1 && !rush_mode) {
    if (aggression == -1) {
        move_score += 3.1 * s->projected_location.get_distance_to(s->location);
				
		// move_score += 2.8 * s->projected_location.get_distance_to(s->entity_target->location);
		// is our target in range?
		double dist_e_target = s->projected_location.get_distance_to(s->entity_target->location);
		
		// move toward the closest enemy.
		double closest_e = 9999;
		for (auto q : s->nearby_enemy_ships) {
			double dist = s->projected_location.get_distance_to(q->location);
			if (dist < closest_e) { closest_e = dist; }
		}
		move_score -= 1.2 * closest_e;
		

        for (hlt::Ship* q : s->nearby_friendly_ships) {
            if (q->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
            double dist;
            if (initial) {
                // dist = s->projected_location.get_distance_to(q->location);
                if (q->entity_id > s->entity_id) { continue; }
                dist = s->projected_location.get_distance_to(q->projected_location);
                // dist = 0;
            } else {
                // if (q->entity_id > s->entity_id) { continue; }
                dist = s->projected_location.get_distance_to(q->projected_location);                
            }

            move_score -= 0.165 * dist;
            if (dist < 1.5) { move_score += 5; }

        }
        
        if (all_nearby_enemy <= nearby_friendly - nearby_enemy) {
            move_score += 5;
        } else {
            move_score -= 2 * all_nearby_enemy;
        }

        for (auto q : s->nearby_enemy_ships) {
            if (q->docking_status == hlt::ShipDockingStatus::Undocked) { continue; }

            double dist = s->projected_location.get_distance_to(q->location);
            if (dist < docked_radius) {
                move_score += 100 - dist;
            }
        }

        return move_score + 1000000;
    }    
    
    // otherwise, run away.
    // if (rush_mode) {
    if (false && rush_mode) {
        for (hlt::Ship* q : s->nearby_enemy_ships) {
            double dist = s->projected_location.get_distance_to(q->location);
            if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
                if (dist <= attack_radius) {
                    if (aggression > 0) {
                        move_score += (-10000 + dist) ;
                    } else {
                        move_score += 10000 - dist;
                        // move_score += 3 * s->speed; // I think this has a disproportionate impact on the scoring function
                    }                
                } else {
                    move_score += (1000 + dist) * aggression;
                }
            } else {
                // docked
                if (dist <= docked_radius) {
                    move_score += (-10000 + dist) * aggression; 
                } else {
                    move_score += (1000 + dist) * aggression;
                }
                
            }
        }

        for (hlt::Ship* q : s->nearby_friendly_ships) {
            double dist;
            if (initial) {
                dist = s->projected_location.get_distance_to(q->location);
            } else {
                dist = s->projected_location.get_distance_to(q->projected_location);                
            }
            move_score -= dist;
        }
    } else {
		move_score += 3.1 * s->projected_location.get_distance_to(s->location);
        move_score -= 1 * s->projected_location.get_distance_to(s->entity_target->location);
        int in_range_count = 0;
        for (hlt::Ship* q : s->nearby_enemy_ships) {
            double dist = s->projected_location.get_distance_to(q->location);
            if (dist > attack_radius) {
                move_score += 2*dist;
            } else {
                move_score += 4*dist;
            }
            
            
            if (dist <= attack_radius) { 
                in_range_count ++;
            }
        }

        if (rush_mode) {
            if (in_range_count == 1) {
                move_score += 50;
            } else {
                move_score -= in_range_count * 50;
            }
        } else {
            if (in_range_count > 1) {
                move_score -= in_range_count * 50;
            }
        }



        if (!rush_mode) {
            int might_attack_count = 0;

            for (auto q : s->nearby_enemy_ships) {
                if (might_attack(s, q)) {
                    might_attack_count++;
                }

                if (might_attack_count > 1) {
                    move_score -= might_attack_count * 600;
                }
                
            }
        }

        for (hlt::Ship* q : s->nearby_friendly_ships) {
            double dist = 0;
            if (initial) {
                // dist = s->projected_location.get_distance_to(q->location);
                if (q->entity_id > s->entity_id) { continue; }
                if (q->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }

                // dist = 0;
                dist = s->projected_location.get_distance_to(q->projected_location);
            } else {
                if ((q->entity_id > s->entity_id) && nav_attempts == 0) { continue; }
                if (q->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }

                dist = s->projected_location.get_distance_to(q->projected_location);                
            }
            move_score -= 0.175 * dist;
			if (dist < 1.5) { move_score += 10; }
        }

        if (num_players == 2) {
            auto c = get_closest_enemy_docked_ship(s);
            if (c != NULL) {
                double dist = c->location.get_distance_to(s->projected_location);
                move_score -= 2 * dist;

                if (dist <= 1 && nearby_enemy > 1 && s->health < 256-64) {
                    s->kamakaze = true;
                    move_score += 1000;
                }
            }
        }


    }

    /*
    if (!initial) {
        // try to group up with friends.
        int friendly_count = 0;
        int sort_size = std::min(6, int(s->nearby_friendly_ships.size()));
        for (int i = 1; i < sort_size; ++i) {
            if (s->nearby_friendly_ships[i]->docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
            
            double d_t = s->projected_location.get_distance_to(s->nearby_friendly_ships[i]->projected_location);
            if (d_t < hlt::constants::SHIP_RADIUS * 2) {
                move_score -= 100000;
            } else {
                if (s->nearby_friendly_ships[i]->behavior != defender) {
                    move_score -= d_t * 0.2;
                    if (d_t <= 2.5) {
                        friendly_count++;
                    }
                }
            }

        }
        move_score += friendly_count * 100;
    }
    */

    return move_score;
}

double Dogfight_Rush::score_move(hlt::Ship* s, bool initial) {

    // will try to avoid all other ships if possible.
    // less of a penalty for docked ships than undocked ships.
    // std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    int aggression = 0;

    int sort_size = std::min(20, int(all_ships.size()));
    std::partial_sort(all_ships.begin(), all_ships.begin() + sort_size, all_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->projected_location.get_distance_to(s->projected_location) < b->projected_location.get_distance_to(s->projected_location); });

    for (int i = 0; i < sort_size; ++i) {
        if (all_ships[i]->projected_location.get_distance_to(s->projected_location) <= hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2) {
            if (all_ships[i]->owner_id == player_id && 2*all_ships[i]->projected_location.get_distance_to(s->projected_location) <= hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2) {
                aggression -= 2;
            } else {
                aggression += 2;
            }
        } else {
            break;
        }
    }

    /*
    if (aggression > 0) {
        aggression = 0;
    } else if (aggression < -6) {
        aggression = -6;
    }
    */

    sort_size = std::min(10, int(enemy_ships.size()));
    std::partial_sort(enemy_ships.begin(), enemy_ships.begin() + sort_size, enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });

    double move_score = -s->projected_location.get_distance_to(s->entity_target->projected_location);

    // double undocked_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 0.45 + aggression;
    double undocked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 0;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 1 + aggression / 2;

    aggression = -1;
    for (hlt::Ship* e : enemy_ships) {

        double dist = s->projected_location.get_distance_to(e->projected_location);
        if (dist > undocked_dist) {
            break;
        }

        if (dist <= hlt::constants::SHIP_RADIUS * 2 + 0.05) {
                move_score -= 100000;
        } else {

            if (e->docking_status == hlt::ShipDockingStatus::Undocked) {
                // move_score += aggression * 2500;
                if (e == s->entity_target) {
                    move_score -= -3* dist * -aggression;
                } else {
                    move_score -= 1000 - dist * -aggression;
                }
            } else {
                if (dist < docked_dist) {
                    if (e == s->entity_target) {
                        move_score -= -1000 - 2 * dist * -aggression;
                    } else {
                        move_score -= 500 - dist * -aggression;
                    }
                }
            }
        }
    }

    // stay close to own ships
    int friendly_count = 0;
    if (!initial) {

        // check if we're close enough to worry about it.
        bool give_bonus = false;
        for (hlt::Ship* f : my_ships) {
            if (f->nearby_enemy_ships.size() > 0) {
                for (hlt::Ship* g : f->nearby_enemy_ships) {
                    if (g->docking_status == hlt::ShipDockingStatus::Undocked) {
                        give_bonus = true;
                        break;
                    }
                }
            }
        }

        if (give_bonus) {
            for (hlt::Ship* f : my_ships) {
                double d_t = s->projected_location.get_distance_to(f->projected_location);
                if (d_t < hlt::constants::SHIP_RADIUS * 2) {
                    move_score -= 100000;
                } else {
                    move_score -= d_t * 1;
                    if (d_t <= 1.5) {
                        ++friendly_count;
                    }
                }
            }
            move_score += friendly_count * 100;
        }
    }


    return move_score;
}

double Dogfight_Rush_4p::score_move(hlt::Ship* s, bool initial) {

    // will try to avoid all other ships if possible.
    // less of a penalty for docked ships than undocked ships.
    // std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });
    
    
    int sort_size = std::min(10, int(enemy_ships.size()));
    std::partial_sort(enemy_ships.begin(), enemy_ships.begin() + sort_size, enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->projected_location) < b->location.get_distance_to(s->projected_location); });

    
    double dist = s->projected_location.get_distance_to(s->entity_target->projected_location);
    
    double move_score = 0;

    if (dist <= hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED * 3) {
        move_score -= dist;
    }

    // double undocked_dist = hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 0.45 + aggression;
    double undocked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2 - 0;
    double docked_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2;

    for (hlt::Ship* e : enemy_ships) {

        double dist = s->projected_location.get_distance_to(e->projected_location);
        if (dist > undocked_dist) {
            break;
        }

        if (dist <= hlt::constants::SHIP_RADIUS * 2 + 0.05) {
            move_score -= 100000;
        } else {

            if (e->docking_status == hlt::ShipDockingStatus::Undocked) {
                // move_score += aggression * 2500;
                if (e == s->entity_target) {
                    move_score -= -3* dist;
                } else {
                    move_score -= 1000 - dist;
                }
            } else {
                if (dist < docked_dist) {
                    if (e == s->entity_target) {
                        move_score -= -1000 - 2 * dist;
                    } else {
                        move_score -= 500 - dist;
                    }
                }
            }
        }
    }

    // stay close to own ships
    int friendly_count = 0;
    if (!initial) {

        // check if we're close enough to worry about it.
        bool give_bonus = false;
        for (hlt::Ship* f : my_ships) {
            if (f->nearby_enemy_ships.size() > 0) {
                for (hlt::Ship* g : f->nearby_enemy_ships) {
                    if (g->docking_status == hlt::ShipDockingStatus::Undocked) {
                        give_bonus = true;
                        break;
                    }
                }
            }
        }

        if (give_bonus) {
            for (hlt::Ship* f : my_ships) {
                double d_t = s->projected_location.get_distance_to(f->projected_location);
                if (d_t < hlt::constants::SHIP_RADIUS * 2) {
                    move_score -= 100000;
                } else {
                    move_score -= d_t * 1;
                    if (d_t <= 1.5) {
                        ++friendly_count;
                    }
                }
            }
            move_score += friendly_count * 100;
        }
    }
    return move_score;
}

double Defense::score_move(hlt::Ship* s, bool initial) {
    // Tries to defend the target by staying as close to it as possible while attacking nearby enemies.

    int sort_size = std::min(10, int(s->nearby_enemy_ships.size()));
    
    double move_score = -s->projected_location.get_distance_to(s->entity_target->location);

    if (move_score <= hlt::constants::SHIP_RADIUS * 2) {
        move_score -= 99999;
    } 

    for (hlt::Ship* e : s->nearby_enemy_ships) {
        if (s->entity_target->location.get_distance_to(e->location) <= hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + hlt::constants::SHIP_RADIUS * 2) {
            move_score -= 0.3 * s->projected_location.get_distance_to(e->location);
            break;
        } else {
            break;
        }
    }
    return move_score;
}

bool Default_Behavior_2p::find_targets(hlt::Ship* s) {

	if (s->docking_status != hlt::ShipDockingStatus::Undocked) {
        return true;
    }

	int enemy_range = 0;
    if (num_players == 2) {
        enemy_range = 7 + s->p_ship_num % 3; // + some random #?
    } else {
        enemy_range = 7 + s->p_ship_num % 3;
    }

    if (enemy_planets.size() == 0 && ally_id != -1 && my_planets.size() > 0) {
        enemy_range = 0;    
    }
	// Default behavior for a 2p bot:
	// 1: If an enemy ship is in range. Fight it.
	// 2: If no enemy ships are in range, find the "best" planet to go to

	if (enemy_ships.size() == 0) {
		// We're going to win next turn?
		s->command = -1;
		hlt::Log::log("All enemy ships are dead...");
        return true;
	}

	hlt::Ship* e_ship = get_closest_enemy_ship(s);
	
	// find the closest friendly planet.
	hlt::Planet* closest_planet;
	double cp_dist = 99999;
	
	for (auto p : my_planets) {
		
		double dist =  s->location.get_distance_to(p->location);
		if (dist < cp_dist && !p->is_full()) { 
			cp_dist = dist;
			closest_planet = p;
		}		
	}
	if (cp_dist != 99999) {
		if (s->can_dock(*closest_planet)) {
			// we want to dock as long as no enemies are near, or we have enough friendlies.
			if (s->nearby_enemy_ships.size() == 0) {
				move_or_dock_to_planet(s, closest_planet, 0.1);
				return true;
			}
			int num_f = 0;
            double c_f = 9999;
			for (auto q : s->nearby_friendly_ships) {
				if (q->docking_status == hlt::ShipDockingStatus::Undocked && (q->command == -1 || q->command == 1)) {
					num_f ++;
                    double dist = q->location.get_distance_to(s->location);
                    if (dist < c_f) { c_f = dist; }
				}
			}
			int num_e = 0;
            double c_e = 9999;
			for (auto q : s->nearby_enemy_ships) {
				if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
					num_e ++;
                    double dist = q->location.get_distance_to(s->location);
                    if (dist < c_e) { c_e = dist; }
				}
			}
			if (num_f >= 1 && (num_e - num_f <= 1)) {
				move_or_dock_to_planet(s, closest_planet, 0.1);
				return true;
			}
		}
	}

	if (s->location.get_distance_to(e_ship->location) <= hlt::constants::MAX_SPEED * enemy_range || neutral_and_owned_planets.size() == 0) {
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " enemy in range.");
		if (enemy_docked_ships.size() == 0 && enemy_attacking_ships.size() > 0) {
			// get the closest attacking ship.
			hlt::Ship* closest_attacking_ship = get_closest_enemy_attacking_ship(s);
			if (s->location.get_distance_to(e_ship->location) > s->location.get_distance_to(closest_attacking_ship->location)) {
				e_ship = closest_attacking_ship;
			}
        } else if (enemy_docked_ships.size() > 0 || enemy_attacking_ships.size() > 0) {
			// get the closest enemy docked or enemy attacking ship.
            //e_ship = get_closest_enemy_docked_or_attacking_ship(s);
            
            auto s1 = get_closest_enemy_attacking_ship(s);

            // target an enemy planet.

            // int planet_index = int(s->rand1 * enemy_planets.size());
            // if (planet_index == enemy_planets.size()) { planet_index--; }
            // hlt::Planet* e_p = enemy_planets[planet_index];
            // hlt::Ship* e_ds = get_closest_enemy_docked_ship_on_planet(s, e_p);

            // if (e_ds == NULL) {
            //     if (s1 != NULL) {
            //         e_ship = s1;
            //     }
            // } else if (s->rand1 < 0.25 && s1 != NULL && 4 * s1->location.get_distance_to(s->location) < e_ds->location.get_distance_to(s->location)) { 
            //     e_ship = s1;
            // } else {
            //     e_ship = e_ds;
            // }

            auto s2 = get_closest_enemy_docked_ship(s);
            auto s3 = get_closest_enemy_undefended_docked_ship(s); 
            
            if (s->rand1 < 0.5 && s1 != NULL && s1->location.get_distance_to(s->location) < s2->location.get_distance_to(s->location)) {
                e_ship = s1;
            } else {
                if (s3 != NULL && num_players != 4) {
                    e_ship = s3;
                } else {
                    e_ship = s2;
                }
            }
            
            // e_ship = enemy_docked_ships[s->p_ship_num % enemy_docked_ships.size()];
        }

		s->command = 1;
		s->entity_target = e_ship;
		s->entity_target->targeted.push_back(s);

		if (false && s->weapon_cooldown == 1) {
			// We can't fire, run away if possible.
		} else {
			if (e_ship->docking_status == hlt::ShipDockingStatus::Undocked) {
				s->target = e_ship->projected_location;
				s->nav = dogfight;
			} else {
				s->target = e_ship->location;                
				s->nav = attack_docked_ship;
                s->nav = dogfight;
			}
		}
	} else {
		// Find a planet to target.
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " no enemy in range.");
        if (s->rand2 < 0.35) {
		    std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
        } else if (s->rand2 < 0.65) {
            std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 * 0.5 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 * 0.5 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
        } else {
            std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 * 0.0 + (a->location.get_distance_to(s->location) )) < (b->score_1 * 0.0 + (b->location.get_distance_to(s->location)));});
        }
		hlt::Planet* planet = neutral_and_owned_planets[0];
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " targeting planet id: " + to_string(planet->entity_id));

		if (planet->owner_id == player_id || !planet->owned) {

			move_or_dock_to_planet(s, planet, 0.1);
			hlt::Log::log("Find_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
			return true;
		} else {
			hlt::Log::log("Ship id: " + to_string(s->entity_id) + " enemy planet targeted");

			std::vector<hlt::Ship*> e_ship_list;
			// Target either the closest enemy docked ship on this planet OR the closest enemy attacking ship.
			hlt::Log::log("Looking for closest enemy docked ship on this planet OR closest enemy attacking ship");
			e_ship = get_closest_enemy_docked_ship_on_planet_or_attacking_ship(s, planet);
			hlt::Log::log("Enemy docked ship " + to_string(e_ship->entity_id) + " targeted, docked on planet " + to_string(planet->entity_id), 10);
			s->command = 1;
			s->entity_target = e_ship;
			s->entity_target->targeted.push_back(s);

            if (e_ship->docking_status == hlt::ShipDockingStatus::Undocked) {
                s->target = e_ship->projected_location;
                s->nav = dogfight;
            } else {
                s->target = e_ship->location;
                s->nav = attack_docked_ship;
                s->nav = dogfight;
            }
			
		}
	}

	hlt::Log::log("Find_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
	return true;
}

bool Default_Behavior_4p::find_targets(hlt::Ship* s) {

    if (s->docking_status != hlt::ShipDockingStatus::Undocked) {
        return true;
    }

    int enemy_range = 4 + s->p_ship_num % 3; // + some random #?

    // Default behavior for a 2p bot:
    // 1: If an enemy ship is in range. Fight it.
    // 2: If no enemy ships are in range, find the "best" planet to go to

    if (enemy_ships.size() == 0) {
        // We're going to win next turn?
        s->command = -1;
        hlt::Log::log("All enemy ships are dead... Ship: " + to_string(s->entity_id));
        return true;
    }

    hlt::Ship* e_ship = get_closest_enemy_ship(s);

    if (s->location.get_distance_to(e_ship->projected_location) <= hlt::constants::MAX_SPEED * enemy_range || neutral_and_owned_planets.size() == 0) {
        hlt::Log::log("Ship id: " + to_string(s->entity_id) + " enemy in range.");

        if (enemy_attacking_ships.size() > 0) {
            hlt::Ship* closest_attacking_ship = get_closest_enemy_attacking_ship(s);
            if (s->location.get_distance_to(e_ship->projected_location) > s->location.get_distance_to(closest_attacking_ship->projected_location)) {
                e_ship = closest_attacking_ship;
            }
            
        //} else if (enemy_docked_ships.size() > 0) {
        //  e_ship = get_closest_enemy_docked_ship(s);
        }

        s->command = 1;
        s->entity_target = e_ship;
        s->entity_target->targeted.push_back(s);
        s->target = e_ship->location;
        s->nav = dogfight;
        
    } else {
        // Find a planet to target.
        hlt::Log::log("Ship id: " + to_string(s->entity_id) + " no enemy in range.");
        std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
        hlt::Planet* planet = neutral_and_owned_planets[0];
        hlt::Log::log("Ship id: " + to_string(s->entity_id) + " targeting planet id: " + to_string(planet->entity_id));

        if (planet->owner_id == player_id || !planet->owned) {

            move_or_dock_to_planet(s, planet, 0.1);
            hlt::Log::log("Find_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
            return true;
        } else {
            hlt::Log::log("Ship id: " + to_string(s->entity_id) + " enemy planet targeted");

            std::vector<hlt::Ship*> e_ship_list;
            // Target either the closest enemy docked ship on this planet OR the closest enemy attacking ship.
            hlt::Log::log("Looking for closest enemy docked ship on this planet OR closest enemy attacking ship");
            e_ship = get_closest_enemy_docked_ship_on_planet_or_attacking_ship(s, planet);
            hlt::Log::log("Enemy docked ship " + to_string(e_ship->entity_id) + " targeted, docked on planet " + to_string(planet->entity_id), 10);
            s->command = 1;
            s->entity_target = e_ship;
            s->entity_target->targeted.push_back(s);
            s->target = e_ship->location;
            s->nav = dogfight;
        }
    }

    hlt::Log::log("Find_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
    return true;
}


bool Rush::find_targets(hlt::Ship* s) {
	if (s->docking_status != hlt::ShipDockingStatus::Undocked) {
        return true;
    }

	hlt::Ship* e_ship = get_closest_enemy_ship(s);
	if (enemy_docked_ships.size() > 0) {
		e_ship = get_closest_enemy_docked_ship(s);
	}

	s->command = 1;
	s->entity_target = e_ship;
	s->entity_target->targeted.push_back(s);
    s->target = e_ship->location;
    s->nav = dogfight;

    // If we have fewer ships OR we have more damage dealt, run away?
    double my_health = 0;
    double enemy_health = 0;

    for (hlt::Ship* ship : my_ships) {
        my_health += ship->projected_health;
    }

    for (hlt::Ship* ship : enemy_ships) {
        enemy_health += ship->projected_health;
    }

    if ( (enemy_ships.size() > my_ships.size()) && (my_health >= enemy_health) && enemy_undocked_ships.size() == 0) {
        // we're outnumbered & we have more damage or same damage. just retreat and hope we are ok.
        s->nav = retreat_from_all;
    }

	hlt::Log::log("rush_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));

	return true;
}

bool Rush_4p::find_targets(hlt::Ship* s) {

    // In a four player game, let's not rush. We don't assume we can rush better and even if we do, we'll end up behind the other two players.
    // However, we still need to be vigilant and not dock if we're threatened.

    // proceed towards the planets like we normally would. 

    if (s->docking_status != hlt::ShipDockingStatus::Undocked) { return true; }
    
    // Is the enemy nearby?

    double attack_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED * 6;

    double dist = 99999;
	double num_docked = 0;

    double enemy_alive = 0;
    double me_alive = 0;
	
	for (hlt::Ship* q : enemy_ships) {
		if (q->owner_id == rush_id && q->docking_status != hlt::ShipDockingStatus::Undocked) {
			num_docked++;
		}
        if (q->owner_id == rush_id) {
            enemy_alive ++;
        }
	}
	
	for (hlt::Ship* q : my_ships) {
		if (q->docking_status != hlt::ShipDockingStatus::Undocked || q->command == 2) {
			num_docked--;
		}
        me_alive++;
	}
	
	for (hlt::Ship* q : enemy_ships) {
		if (q->owner_id == rush_id && q->docking_status != hlt::ShipDockingStatus::Undocked) {
			num_docked++;
		}
	}
	
	for (hlt::Ship* q : my_ships) {
		if (q->docking_status != hlt::ShipDockingStatus::Undocked || q->command == 2) {
			num_docked--;
		}
	}
	
	if (num_docked > 0 && me_alive >= enemy_alive) {
		std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
		hlt::Planet* planet = neutral_and_owned_planets[0];
		hlt::Log::log("Ship id: " + to_string(s->entity_id) + " targeting planet id: " + to_string(planet->entity_id));
		
		move_or_dock_to_planet(s, planet, 0.1);
		return true;
	}		

    // Find the closest enemy ship
    for (hlt::Ship* s2 : my_ships) {    
        std::sort(enemy_ships.begin(), enemy_ships.end(), [s2](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s2->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s2->location); });
        hlt::Ship* e_ship = enemy_ships[0];
        // But we want to prioritize any docked ships if it's the player we're rushing.
        if (enemy_docked_ships.size() > 0) {
            std::sort(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s2](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s2->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s2->location); });
            if (enemy_docked_ships[0]->owner_id == rush_id) {
                e_ship = enemy_docked_ships[0];
                dist = 0;
            }
        }

        double t_dist = s2->location.get_distance_to(e_ship->location);
        if (t_dist < dist) { dist = t_dist; }
    }

    std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s->location); });
    hlt::Ship* e_ship = enemy_ships[0];
    // But we want to prioritize any docked ships if it's the player we're rushing.
    if (enemy_docked_ships.size() > 0) {
        std::sort(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s->location); });
        if (enemy_docked_ships[0]->owner_id == rush_id) {
            e_ship = enemy_docked_ships[0];
        }
    }
    
    if (dist <= attack_dist) {
        // they're coming for us or they've docked. attack.
        s->command = 1;
        s->entity_target = e_ship;
        s->entity_target->targeted.push_back(s);
        s->target = e_ship->location;
        s->nav = dogfight;

        hlt::Log::log("rush_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));
        return true;
    }

    // If the enemy ship is NOT our rush target. run from it.
    auto closest_enemy = get_closest_enemy_ship(s);
    if (closest_enemy->owner_id != rush_id) { 
        s->command = 1;
        s->target = closest_enemy->location;
        s->nav = retreat_from_all;
        return true;
    }

    bool multiple_enemies = false;
    if (s->nearby_enemy_ships.size() > 1) {
        int e_id = s->nearby_enemy_ships[0]->owner_id;
        for (auto q : s->nearby_enemy_ships) {
            if (q->location.get_distance_to(s->location) > hlt::constants::WEAPON_RADIUS + 2 * hlt::constants::MAX_SPEED + hlt::constants::SHIP_RADIUS * 2) { continue; }
            if (q->owner_id != e_id) {
                multiple_enemies = true;
                break;
            }
        }
        
        if (multiple_enemies) {
            // for now, just retreat.
            s->command = 1;
            s->target = closest_enemy->location;
            s->nav = retreat_from_all;
            return true;
        }
    }

    // otherwise, we should move towards the planet(s) we would normally try to dock to. BUT don't dock until we're no longer threatened.

    std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
    hlt::Planet* planet = neutral_and_owned_planets[0];
    hlt::Log::log("Ship id: " + to_string(s->entity_id) + " targeting planet id: " + to_string(planet->entity_id));

    double safe_dock_dist = hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED * 11;
    if (dist >= safe_dock_dist) {
        move_or_dock_to_planet(s, planet, 0.1);
        return true;
    }
    hlt::Log::log("Ship id: " + to_string(s->entity_id) + " navigating to planet");
    s->entity_target = planet;
    s->target = planet->location;
    s->command = 1;
    s->nav = navigate_to_planet_4p_rush;
    planet->targeted.push_back(s);

    hlt::Log::log("Find_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));

    return true;
}

/*
bool Rush_4p::find_targets(hlt::Ship* s) {
    if (s->docking_status != hlt::ShipDockingStatus::Undocked) {
        return true;
    }

    std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s->location); });
    hlt::Ship* e_ship = enemy_ships[0];

    if (enemy_docked_ships.size() > 0) {

        std::sort(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return ((a->owner_id == rush_id) ? -1000 : 1000) + a->location.get_distance_to(s->location) < ((b->owner_id == rush_id) ? -1000 : 1000) + b->location.get_distance_to(s->location); });
        if (enemy_docked_ships[0]->owner_id == rush_id) {
            e_ship = enemy_docked_ships[0];
        }
    }

    s->command = 1;
    s->entity_target = e_ship;
    s->entity_target->targeted.push_back(s);
    if (s->weapon_cooldown == 1 && false) {
        // We can't fire, run away if possible.
    } else {
        if (e_ship->docking_status == hlt::ShipDockingStatus::Undocked) {
            s->target = e_ship->projected_location;
            s->nav = dogfight_rush;
            s->nav = dogfight;
        } else {
            s->target = e_ship->location;
            s->nav = dogfight_rush;
            s->nav = dogfight;
        }
    }

    hlt::Log::log("rush_move: for ship: " + to_string(s->entity_id) + " target_entity_id: " + to_string(s->entity_target->entity_id) + " target x/y: x: " + to_string(s->target.pos_x) + " y: " + to_string(s->target.pos_y));

    return true;
}
*/

double avg_dist_to_enemy_planets(hlt::Planet* p) {
    double distance_to_enemy_planets = 0.0;
    for (hlt::Planet* p2 : enemy_planets) {
        distance_to_enemy_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_enemy_planets = distance_to_enemy_planets / std::max(1, int(enemy_planets.size()));
    return avg_dist_to_enemy_planets;
}

bool Settle::find_targets(hlt::Ship* s) {
    if (neutral_and_owned_planets.size() == 0) { return false; }
    hlt::Log::log("Ship id: " + to_string(s->entity_id) + " finding neutral planet.");

    // std::sort(neutral_planets.begin(), neutral_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});    
    // std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return avg_dist_to_enemy_planets(a) + (a->is_full() ? -1000 : 0)> avg_dist_to_enemy_planets(b) + (b->is_full() ? -1000 : 0) ;});
	// std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_1 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED)) < (b->score_1 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED));});
	
	std::sort(neutral_and_owned_planets.begin(), neutral_and_owned_planets.end(), [s](hlt::Planet* a, hlt::Planet* b) { return (a->score_2 + (a->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED) + (a->is_full() ? 1000 : 0)) < (b->score_2 + (b->location.get_distance_to(s->location) / hlt::constants::MAX_SPEED) + (b->is_full() ? 1000 : 0));});
	
    hlt::Planet* planet = neutral_and_owned_planets[0];
	
	int num_e = 0;
	int num_f = 0;
	
	for (auto q : s->nearby_friendly_ships) {
		if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
			num_f++;
		}
	}
	for (auto q : s->nearby_enemy_ships) {
		if (q->docking_status == hlt::ShipDockingStatus::Undocked) {
			num_e++;
		}
	}	
		
    hlt::Log::log("Ship id: " + to_string(s->entity_id) + " targeting planet id: " + to_string(planet->entity_id));

    // if we're alone, or significantly outnumbered, we shouldn't be docking...
    bool dont_settle = false;
    if (num_f == 0 && num_e > 0) { 
        dont_settle = true;
    } else if (num_e - num_f > 3) {
        dont_settle = true;
    }
    
    if (!rush_mode && dont_settle && (s->can_dock(*planet))) {
        if (planet->owner_id == player_id) {
            move_or_dock_to_planet(s, planet, 0.1);
            return true;
        } else {
            s->behavior = default_behavior_2p;
            return s->behavior->find_targets(s);
        }
    }

	move_or_dock_to_planet(s, planet, 0.1);
	return true;
}

bool Distractor::find_targets(hlt::Ship* s) {
	hlt::Log::log("Attempting distractor move with ship id: " + to_string(s->entity_id));
    hlt::Ship* e_ship;
	if (enemy_docked_ships.size() == 0) {
        //hlt::Log::log("ERROR: get_closest_undocked_enemy_ship returns NULL");
		//return false;
        e_ship = get_closest_enemy_ship(s);

    } else {
        // Move towards a docked ship if possible, but avoid getting into range of any enemy undocked ships.
        e_ship = get_closest_enemy_undefended_docked_ship(s);
    }

	// Move towards a docked ship if possible, but avoid getting into range of any enemy undocked ships.
	// hlt::Ship* e = enemy_docked_ships[s->entity_id % enemy_docked_ships.size()];
    // hlt::Ship* e = get_closest_enemy_undefended_docked_ship(s);
    if (e_ship == NULL) {
        if (enemy_docked_ships.size() > 0) {
            // e_ship = enemy_docked_ships[s->entity_id % enemy_docked_ships.size()];
            e_ship = get_closest_enemy_docked_ship(s);
        } else {
            e_ship = get_closest_enemy_ship(s);
            // e_ship = enemy_docked_ships[s->entity_id % enemy_docked_ships.size()];
        }
    }

	s->command = 1;
	s->entity_target = e_ship;
	s->target = e_ship->location;
    // keep the same target from last turn if we were already in this role

    // std::unordered_map<int, hlt::Ship>::iterator it = last_turn_ships.find(s->entity_id);
    // if (it != last_turn_ships.end()) {
    //     hlt::Ship i = it->second;
    //     if (i.behavior == distractor) {
    //         for (auto q : enemy_docked_ships) {
    //             if (q->entity_id == i.entity_target->entity_id) {
    //                 s->entity_target = q;
    //                 break;
    //             }
    //         }
    //     }
    // }

	s->entity_target->targeted.push_back(s); // do we want this?
	
	s->nav = attack_docked_ship_avoid_all;
    s->nav = dogfight;

	return true;
}


bool Distractor_4p::find_targets(hlt::Ship* s) {
    hlt::Log::log("Attempting distractor move with ship id: " + to_string(s->entity_id));
    hlt::Ship* e_ship;
    if (enemy_docked_ships.size() == 0) {
        // hlt::Log::log("ERROR: get_closest_undocked_enemy_ship returns NULL");
        // return false;
        e_ship = get_closest_enemy_ship(s);

    } else {
        // Move towards a docked ship if possible, but avoid getting into range of any enemy undocked ships.
        e_ship = get_closest_enemy_undefended_docked_ship(s);
    }
    if (e_ship == NULL) {
        if (enemy_docked_ships.size() > 0) {
            e_ship = get_closest_enemy_docked_ship(s);
        } else {
            e_ship = get_closest_enemy_ship(s);
            // e_ship = enemy_docked_ships[s->entity_id % enemy_docked_ships.size()];
        }
    }

    if (s->location.get_distance_to(e_ship->location) > 10*hlt::constants::MAX_SPEED) {
        return false;
    }

    s->command = 1;
    s->entity_target = e_ship;
    s->entity_target->targeted.push_back(s); // do we want this?
    s->target = e_ship->location;
    s->nav = attack_docked_ship_avoid_all;
    s->nav = dogfight;

    return true;

}

bool Defender::find_targets(hlt::Ship* s) {
    if (s->docking_status != hlt::ShipDockingStatus::Undocked) {
        return true;
    }

    if (my_docked_ships.size() == 0) {
        return false;
    }

    std::sort(my_docked_ships.begin(), my_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });

    int r = 1;
    while (r < 4) {
        for (hlt::Ship* s2 : my_docked_ships) {
            if (get_all_enemy_ships_in_range(s2, hlt::constants::MAX_SPEED * r, false).size() > 0) {


                auto nearby_enemy = get_all_enemy_ships_in_range(s2, hlt::constants::MAX_SPEED * r, false);

                s->entity_target = s2;
                s->nav = defense;
                s->command = 1;

                //s->entity_target = nearby_enemy[0];
                //s->nav = dogfight;

                s2->targeted.push_back(s);

                hlt::Log::log("Defense assignment: " + to_string(s->entity_id) + " defending " + to_string(s2->entity_id) + " with enemies in range", 10);

                return true;
            }
        }
        ++r;
    }

    // no ships are threatened. find the docked ship closest to the closest enemy.
    return false;
}