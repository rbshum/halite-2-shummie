#include "filters.h"

hlt::Ship* get_closest_enemy_ship(hlt::Ship* s) {
	//std::sort(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
	//return enemy_ships[0];
	
	return *std::min_element(enemy_ships.begin(), enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
}

hlt::Ship* get_closest_undocked_enemy_ship(hlt::Entity* s) {
    // std::sort(enemy_undocked_ships.begin(), enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
    // return enemy_undocked_ships[0];
	
	return *std::min_element(enemy_undocked_ships.begin(), enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
}

hlt::Ship* get_closest_undocked_enemy_ship(hlt::Location& s) {
    //std::sort(enemy_undocked_ships.begin(), enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s) < b->location.get_distance_to(s); });
    //return enemy_undocked_ships[0];
	
	return *std::min_element(enemy_undocked_ships.begin(), enemy_undocked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s) < b->location.get_distance_to(s); });
}

hlt::Ship* get_closest_enemy_attacking_ship(hlt::Ship* s) {
	if (enemy_attacking_ships.size() == 0) { return NULL; }
	//std::sort(enemy_attacking_ships.begin(), enemy_attacking_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
	//return enemy_attacking_ships[0];
	return *std::min_element(enemy_attacking_ships.begin(), enemy_attacking_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
	
}

hlt::Ship* get_closest_enemy_docked_ship(hlt::Ship* s) {
	if (enemy_docked_ships.size() == 0) { return NULL; }
	//std::sort(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
	//return enemy_docked_ships[0];
	
	return *std::min_element(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
}

hlt::Ship* get_closest_enemy_undefended_docked_ship(hlt::Ship* s) {
    if (enemy_docked_ships.size() == 0) { return NULL; }
    std::sort(enemy_docked_ships.begin(), enemy_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});

    for (hlt::Ship* e : enemy_docked_ships) {
        std::sort(enemy_undocked_ships.begin(), enemy_undocked_ships.end(), [e](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(e->location) < b->location.get_distance_to(e->location);});
        if (e->location.get_distance_to(enemy_undocked_ships[0]->location) > hlt::constants::MAX_SPEED + hlt::constants::WEAPON_RADIUS + 1) {
            return e;
        }
    }

    return NULL;
}

hlt::Ship* get_closest_friendly_ship(hlt::Ship* s) {
	// gets the closest friendly ship to ship s.
	std::sort(my_ships.begin(), my_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
	if (my_ships[0] == s) {
		return my_ships[1];
	}
	return my_ships[0];
}

hlt::Ship* get_closest_friendly_docked_ship(hlt::Ship* s) {
	// gets the closest friendly ship to ship s.
	std::sort(my_docked_ships.begin(), my_docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location); });
	if (my_docked_ships[0] == s) {
		return my_docked_ships[1];
	}
	return my_docked_ships[0];
}


hlt::Ship* get_closest_enemy_docked_or_attacking_ship(hlt::Ship* s) {

	hlt::Ship* s1 = get_closest_enemy_attacking_ship(s);
	hlt::Ship* s2 = get_closest_enemy_docked_ship(s);

	if (enemy_attacking_ships.size() == 0 && enemy_docked_ships.size() == 0) {
		// this function shouldn't be called then...
		return NULL;
	}
	if (enemy_attacking_ships.size() == 0) {
		return s2;
	}
	if (enemy_docked_ships.size() == 0) {
		return s1;
	}
	if (s->location.get_distance_to(s1->location) < s->location.get_distance_to(s2->location)) {
		return s1;
	} else {
		return s2;
	}
}

hlt::Ship* get_closest_enemy_docked_ship_on_planet(hlt::Ship* s, hlt::Planet* p) {
	if (p->docked_ships.size() == 0) { return NULL; }
	std::sort(p->docked_ships.begin(), p->docked_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s->location) < b->location.get_distance_to(s->location);});
	return p->docked_ships[0];
}

hlt::Ship* get_closest_enemy_docked_ship_on_planet_or_attacking_ship(hlt::Ship* s, hlt::Planet* p) {
	hlt::Ship* s1 = get_closest_enemy_attacking_ship(s);
	hlt::Ship* s2 = get_closest_enemy_docked_ship_on_planet(s, p);

	if (enemy_attacking_ships.size() == 0 && p->docked_ships.size() == 0) {
		// this function shouldn't be called then...
		return NULL;
	}
	if (enemy_attacking_ships.size() == 0) {
		return s2;
	}
	if (p->docked_ships.size() == 0) {
		return s1;
	}
	if (s->location.get_distance_to(s1->location) < s->location.get_distance_to(s2->location)) {
		return s1;
	} else {
		return s2;
	}
}

std::vector<hlt::Ship*> get_all_enemy_ships_in_range(hlt::Ship* s, double distance, bool already_sorted) {
	std::vector<hlt::Ship*> nearby_ships;

	// We MIGHT be able to assume that enemy_ships is sorted by distance already... But, why take that chance?
	if (already_sorted) {
		for (hlt::Ship* e : enemy_ships) {
			if (e->location.get_distance_to(s->location) <= distance) {
				nearby_ships.push_back(e);
			} else {
				return nearby_ships;
			}
		}
	} else {
		for (hlt::Ship* e : enemy_ships) {
			if (e->location.get_distance_to(s->location) <= distance) {
				nearby_ships.push_back(e);
			}
		}
	}
	return nearby_ships;
}