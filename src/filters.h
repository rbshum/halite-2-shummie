#pragma once

#include "filters.h"
#include "../hlt/globals.h"
#include "../hlt/hlt.hpp"
#include "../hlt/log.hpp"


hlt::Ship* get_closest_enemy_ship(hlt::Ship* s);
hlt::Ship* get_closest_enemy_attacking_ship(hlt::Ship* s);
hlt::Ship* get_closest_undocked_enemy_ship(hlt::Entity* s);
hlt::Ship* get_closest_undocked_enemy_ship(hlt::Location& s);
hlt::Ship* get_closest_enemy_docked_ship(hlt::Ship* s);
hlt::Ship* get_closest_enemy_docked_or_attacking_ship(hlt::Ship* s);
hlt::Ship* get_closest_enemy_docked_ship_on_planet(hlt::Ship* s, hlt::Planet* p);
hlt::Ship* get_closest_enemy_docked_ship_on_planet_or_attacking_ship(hlt::Ship* s, hlt::Planet* p);
hlt::Ship* get_closest_friendly_ship(hlt::Ship* s);
hlt::Ship* get_closest_friendly_docked_ship(hlt::Ship* s);
hlt::Ship* get_closest_enemy_undefended_docked_ship(hlt::Ship* s);

std::vector<hlt::Ship*> get_all_enemy_ships_in_range(hlt::Ship* s, double distance=hlt::constants::MAX_SPEED, bool already_sorted=true);