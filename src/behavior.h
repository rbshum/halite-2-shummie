#pragma once

// #include "base_functions.h"
#include "filters.h"
// #include "../hlt/hlt.hpp"
#include "../hlt/log.hpp"
#include "../hlt/ship.hpp"
#include "logic.h"

class Navigation {
public:
    virtual std::string name() = 0;
    virtual double score_move(hlt::Ship* s, bool initial) = 0;

    //Navigation(hlt::Ship* s);
};

class Default_Navigation : public Navigation {
public:
    std::string name() { return "Default_Navigation"; }
    double score_move(hlt::Ship* s, bool initial);
    //Default_Navigation(hlt::Ship* s);
};

class Coward : public Navigation {
public:
    std::string name() { return "Coward"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Navigate_To_Planet : public Navigation {
public:
    std::string name() { return "Navigate_To_Planet"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Navigate_To_Planet_4p_Rush : public Navigation {
public:
    std::string name() { return "Navigate_To_Planet_4p_Rush"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Attack_Closest_Keep_Away : public Navigation {
public:
    //Attack_Closest_Keep_Away(hlt::Ship* s);
    std::string name() { return "Attack_Closest_Keep_Away"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Attack_Docked_Keep_Away : public Navigation {
public:
    //Attack_Docked_Keep_Away();
    std::string name() { return "Attack_Docked_Keep_Away"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Retreat_From_All : public Navigation {
public:
    std::string name() { return "Retreat_From_All"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Retreat_From_All_Group_Friendly : public Navigation {
public:
    std::string name() { return "Retreat_From_All_Group_Friendly"; }
    double score_move(hlt::Ship* s, bool initial);
};


class Attack_Docked_Ship : public Navigation {
public:
    std::string name() { return "Attack_Docked_Ship"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Attack_Docked_Ship_Avoid_All : public Navigation {
public:
    std::string name() { return "Attack_Docked_Ship_Avoid_All"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Dogfight : public Navigation {
public:
    std::string name() { return "Dogfight"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Dogfight_Rush : public Navigation {
public:
    std::string name() { return "Dogfight_Rush"; }
    double score_move(hlt::Ship* s, bool initial);
};

class Dogfight_Rush_4p : public Navigation {
public:
    std::string name() { return "Dogfight_Rush_4p"; }
    double score_move(hlt::Ship* s, bool initial);
};


class Defense : public Navigation {
public:
    std::string name() { return "Defense"; }
    double score_move(hlt::Ship* s, bool initial);
};


class Behavior {
public:
    virtual std::string name() = 0;
	virtual bool find_targets(hlt::Ship* s) = 0;
};


class Default_Behavior_2p : public Behavior {
public:
    std::string name() { return "Default_Behavior_2p"; }
	bool find_targets(hlt::Ship* s);
};

class Default_Behavior_4p : public Behavior {
public:
    std::string name() { return "Default_Behavior_4p"; }
    bool find_targets(hlt::Ship* s);
};

class Rush : public Behavior {
public:
    std::string name() { return "Rush"; }
	bool find_targets(hlt::Ship* s);
};

class Rush_4p : public Behavior {
public:
    std::string name() { return "Rush"; }
    bool find_targets(hlt::Ship* s);
};

class Settle : public Behavior {
public:
    std::string name() { return "Settle"; }
	bool find_targets(hlt::Ship* s);
};

class Distractor : public Behavior {
public:
    std::string name() { return "Distractor"; }
	bool find_targets(hlt::Ship* s);
};

class Distractor_4p : public Behavior {
public:
    std::string name() { return "Distractor_4p"; }
    bool find_targets(hlt::Ship* s);
};

class Defender : public Behavior {
public:
    std::string name() { return "Defender"; }
    bool find_targets(hlt::Ship* s);
};
