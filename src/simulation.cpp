#include "simulation.hpp"


void process_docking(hlt::Map* game) {
    // update docking/undocking status
    for (int i = 0; i < num_players; ++i) {
        auto& player_ships = game->ships.at(i);
        for (auto& ship : player_ships) {

            if (ship.docking_status == hlt::ShipDockingStatus::Docking) {
                ship.docking_progress--;
                if (ship.docking_progress == 0) {
                    ship.docking_status = hlt::ShipDockingStatus::Docked;
                }
            } else if (ship.docking_status == hlt::ShipDockingStatus::Undocking) {
                ship.docking_progress--;
                if (ship.docking_progress == 0) {
                    ship.docking_status = hlt::ShipDockingStatus::Undocked;
                    auto& planet = game->planets.at(ship.docked_planet);
                    planet.remove_ship(ship);
                }
            }
        }
    }

    for (auto& p : game->planets) {
        p.frozen = false;
    }
}

void process_docking_move(hlt::Map* game, hlt::Ship& ship, hlt::Planet& planet) {
    // going to ignore error checking.
    // Moves will assume that we can dock and that the planet is alive.

    // For now, also don't simulate multiple players docking on the same planet.
    if (!planet.owned) {
        planet.owned = true;
        planet.owner_id = ship.owner_id;
        planet.current_production = 0;
    }
    if (planet.owner_id == ship.owner_id && int(planet.docked_ships.size()) < planet.docking_spots) {
        ship.docked_planet = planet.entity_id;
        ship.docking_status = hlt::ShipDockingStatus::Docking;
        ship.docking_progress = hlt::constants::DOCK_TURNS;
        planet.add_ship(ship);
    }
    // ignoring simulateous docking combat

}

void process_moves(hlt::Map* game) {
    // keep track of which ships docked simultaneously
    // std::vector<hlt::Ship*> sim_docking; // vector didn't right

    for (int i = 0; i < num_players; ++i) {
        auto& player_ships = game->ships.at(i);
        for (auto& ship : player_ships) {
            switch (ship.command) {
                case -1:
                    break;
                case 1: {
                    // thrust
                    if (ship.docking_status != hlt::ShipDockingStatus::Undocked) {
                        break;
                    }

                    ship.vx = cos(ship.angle * M_PI / 180) * ship.speed;
        			ship.vy = sin(ship.angle * M_PI / 180) * ship.speed;
                    break;
                }
                case 2: {
                    // dock
                    process_docking_move(game, ship, game->get_planet(ship.entity_target->entity_id));
                    break;
                }
                case 3: {
                    // undock
                    if (ship.docking_status != hlt::ShipDockingStatus::Docked) {
                        break;
                    }
                    ship.docking_status = hlt::ShipDockingStatus::Undocking;
                    ship.docking_progress = hlt::constants::DOCK_TURNS;
                    break;
                }
            }
            // extra code here: https://github.com/HaliteChallenge/Halite-II/blob/master/environment/core/Halite.cpp#L513

        }
    }
}


bool might_attack(double dist2, const hlt::Ship& s1, const hlt::Ship& s2) {
    return dist2 <= (s1.speed + s2.speed + s1.radius + s2.radius + hlt::constants::WEAPON_RADIUS) * (s1.speed + s2.speed + s1.radius + s2.radius + hlt::constants::WEAPON_RADIUS);
}

bool might_collide(double dist2, const hlt::Ship& s1, const hlt::Ship& s2) {
    return dist2 <= (s1.speed + s2.speed + s1.radius + s2.radius) * (s1.speed + s2.speed + s1.radius + s2.radius);
}

double round_event_time(double t) {
    return std::round((t * EVENT_TIME_PRECISION) / EVENT_TIME_PRECISION);
}




std::pair<bool, double> coll_t(double r, const hlt::Entity& e1, const hlt::Entity& e2) {
    const double dx = e1.location.pos_x - e2.location.pos_x;
    const double dy = e1.location.pos_y - e2.location.pos_y;
    const double dvx = e1.vx - e2.vx;
    const double dvy = e1.vy - e2.vy;

    const double a = dvx * dvx + dvy * dvy;
    const double b = 2 * (dx * dvx + dy * dvy);
    const double c = dx * dx + dy * dy - r * r;

    const double disc = b*b - 4*a*c;

    if (a == 0) {
        if (b == 0) {
            if (c == 0) {
                return {true, 0.0};
            }
            return {false, 0.0};
        }
        const double t = -c / b;
        if (t >= 0.0) {
            return {true, t};
        }
        return {false, 0.0};
    } else if (disc == 0.0) {
        const double t = -b / (2 * a);
        return {true, t};
    } else if (disc > 0) {
        const double t1 = -b + std::sqrt(disc);
        const double t2 = -b - std::sqrt(disc);

        if (t1 > 0.0 && t2 > 0.0) {
            return {true, std::min(t1, t2) / (2*a)};
        } else if (t1 <= 0.0 && t2 <= 0.0) {
            return {true, std::max(t1, t2) / (2 * a)};
        } else {
            return {true, 0.0};
        }
    } else {
        return {false, 0.0};
    }
}

void find_events(std::unordered_set<Simulation_Event>& unsorted_events, hlt::Ship& s1, hlt::Ship& s2) {
    
    const double dist2 = s1.location.distance2(s2.location);

    if (s1.owner_id != s2.owner_id && might_attack(dist2, s1, s2)) {
        // combat event
        const double attack_radius = s1.radius + s2.radius + hlt::constants::WEAPON_RADIUS;
        const std::pair<bool, double> t = coll_t(attack_radius, s1, s2);
        if (t.first && t.second >= 0.0 && t.second <= 1.0) {
            unsorted_events.insert(Simulation_Event{1, &s1, &s2, round_event_time(t.second)});
        } else if (dist2 < attack_radius * attack_radius) {
            unsorted_events.insert(Simulation_Event{1, &s1, &s2, 0});
        }
    }

    if (s1.entity_id != s2.entity_id && might_collide(dist2, s1, s2)) {
        const double collision_radius = s1.radius + s2.radius;
        const std::pair<bool, double> t = coll_t(collision_radius, s1, s2);
        if (t.first) {
            if (t.second >= 0 && t.second <= 1.0) {
                unsorted_events.insert(Simulation_Event{2, &s1, &s2, round_event_time(t.second)});
            }
        }
    }

}

void process_events(hlt::Map* game) {
    std::unordered_set<Simulation_Event> unsorted_events;

    /*
    const auto event_horizon = [](const hlt::Ship& ship) -> double {
        // The size of the ship's event horizon
        return ship.radius + ship.speed + hlt::constants::WEAPON_RADIUS;
    };
    */

    // Collision_Map collision_map = Collision_Map(*game, event_horizon);
    // std::vector<int> potential_collisions;

    for (int i = 0; i < num_players; ++i) {
        for (int ia = 0; ia < int(game->ships.at(i).size()); ++ia) {
            hlt::Ship& ship = game->ships.at(i)[ia];
            for (int ja = ia + 1; ja < int(game->ships.at(i).size()); ++ja) {
                hlt::Ship& ship2 = game->ships.at(i)[ja];
                find_events(unsorted_events, ship, ship2);
            }            
            for (int j = i + 1; j < num_players; ++j) {
                // we don't need to loop through every player before since we would have caught the collision earlier
                for (auto& ship2 : game->ships.at(j)) {
                    find_events(unsorted_events, ship, ship2);
                }
            }

            // possible ship-planet collisions
            for (hlt::Planet& planet : game->planets) {
                const double dist2 = ship.location.distance2(planet.location);
                if (dist2 <= (ship.speed + ship.radius + planet.radius) * (ship.speed + ship.radius + planet.radius)) {
                    const double collision_radius = ship.radius + planet.radius;
                    const auto t = coll_t(collision_radius, ship, planet);
                    if (t.first) {
                        if (t.second >= 0.0 && t.second <= 1.0) {
                            unsorted_events.insert(Simulation_Event{2, &ship, &planet, round_event_time(t.second)});
                        }
                    }
                }
            }
            
            // find situations where ships go off the map
            auto final_location = ship.location;
            final_location.pos_x += ship.vx;
            final_location.pos_y += ship.vy;

            if (final_location.pos_x < 0 || final_location.pos_y < 0 || final_location.pos_x >= game->map_width || final_location.pos_y >= game->map_height) {
                double time = 1000000.0;
                if (ship.vx != 0) {
                    const double t1 = -ship.location.pos_x / ship.vx;
                    if (t1 < time && t1 >= 0.0) time = t1;
                    const double t2 = (game_map->map_width - ship.location.pos_x) / ship.vx;
                    if (t2 < time && t2 >= 0.0) time = t2;
                }
                if (ship.vy != 0) {
                    const double t3 = -ship.location.pos_y / ship.vy;
                    if (t3 < time && t3 >= 0.0) time = t3;
                    const double t4 = (game_map->map_height - ship.location.pos_y) / ship.vy;
                    if (t4 < time && t4 >= 0.0) time = t4;
                }

                unsorted_events.insert(Simulation_Event{3, &ship, &ship, round_event_time(time)});
            }
        }
    }
}

// takes a hlt::Map object and runs a full turn simulation on it.
// certain characteristics of the entities within the Map object must be set otherwise the simulation won't run properly
// returns a new Map object with the new gamestate

hlt::Map simulate_turn(hlt::Map* game) {

    // Create a new map object that will be a copy of the current game state. This might be inefficient...
    hlt::Map next_turn = hlt::Map(*game);

    // Turns are calculated using the following order of steps:
    // 1. The status of any docked ships is updated.
    process_docking(&next_turn);

    // 2. Player commands are processed. For instance, a new thrust command will instantaneously update the ship velocity.
    process_moves(&next_turn);

    // 3. Movement, collisions, and attacks are resolved simultaneously
    process_events(&next_turn);


    return next_turn;
}