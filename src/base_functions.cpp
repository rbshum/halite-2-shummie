#include "base_functions.h"

void save_last_game_ships() {

    for (auto& player_v_pair : game_map->ships) {
		for (hlt::Ship& s : player_v_pair.second) {
			last_turn_ships[s.entity_id] = s;
		}
	}
}

void process_last_turn_ship_data() {

	// goes through each ENEMY ship ALIVE and updates some info based on last turn's info.

	// for now, assume that the ships move in the same velocity they were last turn.
	// IGNORES collision.

	for (hlt::Ship* s : enemy_ships) {
		std::unordered_map<int, hlt::Ship>::iterator it = last_turn_ships.find(s->entity_id);
		if (it != last_turn_ships.end()) {
			hlt::Ship i = it->second;
			// If this ship's weapon cooldown is currently 1 (what if ships try to retreat after firing?)
			double speed = round(sqrt((s->location.pos_x - i.location.pos_x) * (s->location.pos_x - i.location.pos_x) + (s->location.pos_y - i.location.pos_y) * (s->location.pos_y - i.location.pos_y)));
			int angle = i.location.angle_to(s->location);
			s->speed = speed;
            if (s->weapon_cooldown == 2) {
                angle = (angle + 180) % 360;
            }
			s->angle = angle;
			s->update();
			hlt::Log::log("Enemy ship: " + to_string(s->entity_id) + " prior x/y: " + to_string(i.location.pos_x) + "/" + to_string(i.location.pos_y) + " current x/y: " + to_string(s->location.pos_x) + "/" + to_string(s->location.pos_y) + " speed: " + to_string(s->speed) + " angle: " + to_string(s->angle));
			// s->vx = s->location.pos_x - i.location.pos_x;
			// s->vy = s->location.pos_y - i.location.pos_y;
		} else {
			// just to make sure projected location is updated with current position.
			s->update();
		}
	}

    // this may or may not belong here but update all of our projected vectors.

    for (hlt::Ship* s : my_ships) {
        s->update();

        // check to see if we need a new seed

        std::unordered_map<int, hlt::Ship>::iterator it = last_turn_ships.find(s->entity_id);
		if (it != last_turn_ships.end()) {
            hlt::Ship i = it->second;
            s->rand1 = i.rand1;
            s->rand2 = i.rand2;
        } else {
            s->rand1 = random_real(eng);
            s->rand2 = random_real(eng);
        }

    }
}

void check_ally_signal() {
    // check for ally signal    

    if (num_players != 4 || turn != 2) { return; }
    hlt::Log::log("Checking for ally.");

    for (auto& player_v_pair : game_map->ships) {
        if (player_v_pair.first != player_id) {
            bool angle_9 = true;
            bool speed_6 = false;
            // Ship(PID * 3 + PID % 3) moves 6 first turn
	        // Ship((PID * 3 + 1) % 3) angle = 0
            int speed_6_ship = player_v_pair.first * 3 + player_v_pair.first % 3;            

            hlt::Log::log("Player: " + to_string(player_v_pair.first) + " Speed_6_ship: " + to_string(speed_6_ship));

            for (hlt::Ship& s : player_v_pair.second) {
                if (s.entity_id == speed_6_ship) {
                    if (int(s.speed) == 6) {
                        hlt::Log::log("Speed 6 ship found: Ship entityid: " + to_string(s.entity_id));
                        speed_6 = true;
                    }
                }

                int angle_check = (player_v_pair.first + s.entity_id) % 9;
                if (s.angle % 9 != angle_check) {
                    angle_9 = false;
                    break;
                }
                
            }

            if (speed_6 && angle_9) {
                ally_id = player_v_pair.first;
				// assert(false);
                break;
            }
        }
    }

}

void update_planet_data() {

    hlt::Log::log("updating planet data.", 10);

    // based off of the last turn game data and the current game data, update stateful planet data.
    for (hlt::Planet& p : game_map->planets) {
        hlt::Planet& last_p = last_turn_map->get_planet(p.entity_id);

        // Start by populating the resources from the prior turn.
        if (turn == 1) {
            // set resources to 0
            p.resources = 0;
        } else {
            p.resources = last_p.resources;
        }

        // check the planet for docked ships. Ships that are fully docked at the beginning of this turn would have contributed to the mined resources of the planet.
        for (hlt::Ship* s : p.docked_ships) {
            // docked ships is actually a list of all docked, docking and undocking ships
            if (s->docking_status == hlt::ShipDockingStatus::Docked) {
                p.resources += hlt::constants::BASE_PRODUCTIVITY;
            }
        }

        // If the planet has >= 72 resources, then that means a ship was produced last turn
        if (p.resources >= 72) {
            // we don't need to worry about generating more than 72 resources a turn
            p.resources -= 72;
        }
    }

}

void create_filtered_vectors(int player) {
    hlt::Log::log("Resetting planet vectors");

    all_planets.clear();
    enemy_planets.clear();
    my_planets.clear();
    ally_planets.clear();
    neutral_planets.clear();
    my_planets_dockable.clear();
    neutral_and_owned_planets.clear();
    interesting_planets.clear();

    for (auto& p : game_map->planets) {
        hlt::Log::log("Planet: " + to_string(p.entity_id), 5);
        hlt::Log::log("Owner: " + to_string(p.owner_id), 5);
        all_planets.push_back(&p);
        hlt::Log::log("   Added to all_planets", 11);
        if (p.owner_id == -1) {
            neutral_planets.push_back(&p);
            hlt::Log::log("   Added to neutral_planets", 11);
            neutral_and_owned_planets.push_back(&p);
            hlt::Log::log("   Added to neutral_and_owned_planets", 11);
            interesting_planets.push_back(&p);
            hlt::Log::log("   Added to interesting_planets", 11);
        } else if (p.owner_id == player) {
            my_planets.push_back(&p);
            hlt::Log::log("   Added to my_planets", 11);
            if (!p.is_full()) {
                my_planets_dockable.push_back(&p);
                hlt::Log::log("   Added to my_planets_dockable", 11);
                neutral_and_owned_planets.push_back(&p);
                hlt::Log::log("   Added to neutral_and_owned_planets", 11);
                interesting_planets.push_back(&p);
                hlt::Log::log("   Added to interesting_planets", 11);
            }
        } else {
            enemy_planets.push_back(&p);
            hlt::Log::log("   Added to enemy_planets", 11);
            interesting_planets.push_back(&p);
            hlt::Log::log("   Added to interesting_planets", 11);
        }
    }

    all_ships.clear();
    enemy_ships.clear();
    my_ships.clear();
    ally_ships.clear();
    enemy_undocked_ships.clear();
    enemy_docked_ships.clear();
    my_undocked_ships.clear();
    my_docked_ships.clear();
    ally_undocked_ships.clear();
    ally_docked_ships.clear();
    enemy_attacking_ships.clear();

    for (auto& player_v_pair : game_map->ships) {
        if (player_v_pair.first == player) {
            for (hlt::Ship& s : player_v_pair.second) {
                if (s.projected_health > 0) {
                    all_ships.push_back(&s);
                    my_ships.push_back(&s);
                    if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                        my_undocked_ships.push_back(&s);
                    } else {
                        my_docked_ships.push_back(&s);
                    }
                }
            }
        } else {
            for (hlt::Ship& s : player_v_pair.second) {
                if (s.projected_health > 0) {
                    all_ships.push_back(&s);
                    enemy_ships.push_back(&s);
                    if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                        enemy_undocked_ships.push_back(&s);
                    } else {
                        enemy_docked_ships.push_back(&s);
                    }
                }
            }
        }
    }

    for (hlt::Ship* s : enemy_ships) {
        for (hlt::Ship* s2 : my_docked_ships) {
            if (s->location.get_distance_to(s2->location) <= hlt::constants::MAX_SPEED * 4) {
                enemy_attacking_ships.push_back(s);
                break;
            }
        }
    }
}

void create_filtered_vectors_ally(int player) {
    hlt::Log::log("Resetting planet vectors");

    all_planets.clear();
    enemy_planets.clear();
    my_planets.clear();
    neutral_planets.clear();
    ally_planets.clear();
    my_planets_dockable.clear();
    neutral_and_owned_planets.clear();
    interesting_planets.clear();

    for (auto& p : game_map->planets) {
        hlt::Log::log("Planet: " + to_string(p.entity_id), 5);
        hlt::Log::log("Owner: " + to_string(p.owner_id), 5);
        if (p.owner_id != ally_id) {
            all_planets.push_back(&p);
            hlt::Log::log("   Added to all_planets", 11);
        }        
        if (p.owner_id == -1) {
            neutral_planets.push_back(&p);
            hlt::Log::log("   Added to neutral_planets", 11);
            neutral_and_owned_planets.push_back(&p);
            hlt::Log::log("   Added to neutral_and_owned_planets", 11);
            interesting_planets.push_back(&p);
            hlt::Log::log("   Added to interesting_planets", 11);
        } else if (p.owner_id == player) {
            my_planets.push_back(&p);
            hlt::Log::log("   Added to my_planets", 11);
            if (!p.is_full()) {
                my_planets_dockable.push_back(&p);
                hlt::Log::log("   Added to my_planets_dockable", 11);
                neutral_and_owned_planets.push_back(&p);
                hlt::Log::log("   Added to neutral_and_owned_planets", 11);
                interesting_planets.push_back(&p);
                hlt::Log::log("   Added to interesting_planets", 11);
            }
        } else if (p.owner_id != ally_id) {
            enemy_planets.push_back(&p);
            hlt::Log::log("   Added to enemy_planets", 11);
            interesting_planets.push_back(&p);
            hlt::Log::log("   Added to interesting_planets", 11);
        } else if (p.owner_id == ally_id) {
            ally_planets.push_back(&p);            
        }
    }

    all_ships.clear();
    enemy_ships.clear();
    my_ships.clear();
    ally_ships.clear();
    enemy_undocked_ships.clear();
    enemy_docked_ships.clear();
    my_undocked_ships.clear();
    my_docked_ships.clear();
    ally_undocked_ships.clear();
    ally_docked_ships.clear();
    enemy_attacking_ships.clear();

    for (auto& player_v_pair : game_map->ships) {
        if (player_v_pair.first == player) {
            for (hlt::Ship& s : player_v_pair.second) {
                if (s.projected_health > 0) {
                    all_ships.push_back(&s);
                    my_ships.push_back(&s);
                    if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                        my_undocked_ships.push_back(&s);
                    } else {
                        my_docked_ships.push_back(&s);
                    }
                }
            }
        } else if (player_v_pair.first != ally_id) {
            for (hlt::Ship& s : player_v_pair.second) {
                if (s.projected_health > 0) {
                    all_ships.push_back(&s);
                    enemy_ships.push_back(&s);
                    if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                        enemy_undocked_ships.push_back(&s);
                    } else {
                        enemy_docked_ships.push_back(&s);
                    }
                }
            }
        } else if (player_v_pair.first == ally_id) {
            for (hlt::Ship& s : player_v_pair.second) {
                if (s.projected_health > 0) {                    
                    ally_ships.push_back(&s);
                    if (s.docking_status == hlt::ShipDockingStatus::Undocked) {
                        ally_undocked_ships.push_back(&s);
                    } else {
                        ally_docked_ships.push_back(&s);
                        // A HACK to avoid attacking them
                        s.radius = hlt::constants::SHIP_RADIUS * 3 + hlt::constants::WEAPON_RADIUS;
                    }
                }                
            }
        }
    }

    for (hlt::Ship* s : enemy_ships) {
        for (hlt::Ship* s2 : my_docked_ships) {
            if (s->location.get_distance_to(s2->location) <= hlt::constants::MAX_SPEED * 4) {
                enemy_attacking_ships.push_back(s);
                break;
            }
        }
    }
}

void simulate_start_of_turn() {
	// Identify the ships that will still be alive at the start of the next turn.
	// If a ship is in range of another ship at the beginning of the turn, then it will take damage.
	// Do we update the state of the game or just leave filters on? Also, do we change people's health??

	// for each ship, if there are enemy ships in range, apply the damage.
	// For all intents and purposes, if a ship is going to be immediately destroyed, then we should be able to ignore that ship.

	// THIS CODE IS GOING TO BE UNSTABLEEEEEEE

	for (auto& player_v_pair : game_map->ships) {

		// Check if there are any enemy ships in range. Note an "enemy" ship is any ship not owned by this player.
		for (hlt::Ship& s : player_v_pair.second) {
            if (s.docking_status != hlt::ShipDockingStatus::Undocked) { continue; }
			std::vector<hlt::Ship*> ships_in_weapon_range = get_all_enemy_ships_in_weapon_range(&s);

			if (ships_in_weapon_range.size() == 0) { continue; }

			double damage_to_each_ship = hlt::constants::WEAPON_DAMAGE / ships_in_weapon_range.size();

			s.weapon_cooldown = 1; // TODO: Check is this true? Do ships get initialized with 0? Right now we force this to 0 in hlt_in.hpp
			for (hlt::Ship* e : ships_in_weapon_range) {
				e->projected_health -= damage_to_each_ship;
			}
		}
    }

    // We could kill the ship off here, but we'll just filter it out of our ship vectors instead.
}


void populate_nearby_ships() {

    // populates the nearby_friendly_ships and nearby_enemy_ships for each ship
    // friendly/enemy is defined as relative to the ship.

    // all ships within dist will be added.
    double dist = hlt::constants::SHIP_RADIUS * 2 + hlt::constants::WEAPON_RADIUS + hlt::constants::MAX_SPEED * 2;

    for (auto& player_v_pair : game_map->ships) {
        for (hlt::Ship& s : player_v_pair.second) {

            for (auto& player_v_pair_2 : game_map->ships) {
                if (player_v_pair_2.first == player_v_pair.first) {
                    for (hlt::Ship& s2 : player_v_pair_2.second) {
                        double d = s.location.get_distance_to(s2.location);
                        if (d <= dist && d != 0) {
                            s.nearby_friendly_ships.push_back(&s2);
                        }
                    }
                } else {
                    for (hlt::Ship& s2 : player_v_pair_2.second) {
                        if (s.location.get_distance_to(s2.location) <= dist) {
                            s.nearby_enemy_ships.push_back(&s2);
                        }
                    }                    
                }
            }

            // sort the vectors by distance, closest to furthest            
            std::sort(s.nearby_enemy_ships.begin(), s.nearby_enemy_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s.location) < b->location.get_distance_to(s.location); });
            std::sort(s.nearby_friendly_ships.begin(), s.nearby_friendly_ships.end(), [s](hlt::Ship* a, hlt::Ship* b) { return a->location.get_distance_to(s.location) < b->location.get_distance_to(s.location); });
        }
    }

}


std::vector<hlt::Ship*> get_all_enemy_ships_in_weapon_range(hlt::Ship* s) {

	// gets all enemy ships that are within weapon range of this ship.
	// Note, weapon range is actually: EDGE to EDGE.
	std::vector<hlt::Ship*> ships_in_weapon_range;

	// From a ship at position (x, y), we add the radius to get to the edge, then weapon-radius, and then another radius to get to all ships that are in range.
	// This is a fairly expensive operation as we're looping through all ships for every ship. There might be more efficient ways to do this. Might want to add a custom timer to track how long this function takes.
	double reach = (hlt::constants::SHIP_RADIUS * 2 + hlt::constants::WEAPON_RADIUS) * (hlt::constants::SHIP_RADIUS * 2 + hlt::constants::WEAPON_RADIUS);

	for (auto& player_v_pair : game_map->ships) {
		if (player_v_pair.first == s->owner_id) { continue; } // only need to check enemy ships.

		for (hlt::Ship& s2 : player_v_pair.second) {
			double dx = s2.location.pos_x - s->location.pos_x;
			double dy = s2.location.pos_y - s->location.pos_y;
			double d2 = dx * dx + dy * dy;
			if (d2 <= reach) {
				ships_in_weapon_range.push_back(&s2);
			}
		}
	}
	return ships_in_weapon_range;
}


double score_planet_settle_2(hlt::Planet* p) {
	
	// LOWER scores are MORE valuable.
	
	// determines which planets we should try to settle.
	double score = 0;
	score = p->docking_spots * -1;
	if (p->owner_id == player_id) { score += 1; }
	
	double min_enemy = 9999;
    for (hlt::Planet* p2 : enemy_planets) {
        double dist = p->location.get_distance_to(p2->location);
        if (dist < min_enemy) { min_enemy = dist; }
    }

    double min_me = 9999;
    for (hlt::Planet* p2 : my_planets) {        
        double dist = p->location.get_distance_to(p2->location);
        if (dist < min_me) { min_me = dist; }
    }

	return (score + min_me - 1.25 * min_enemy) * 1.25;
    
}


void score_all_planets() {
    // score planets.
    for (hlt::Planet* p : all_planets) {
        p->score_1 = score_planet(p);
		p->score_2 = score_planet_settle(p);
    }
}

double score_planet_settle(hlt::Planet* p) {
	
	// LOWER scores are MORE valuable.
	
	// determines which planets we should try to settle.
	double score = 0;
	score = p->docking_spots * -1;
	if (p->owner_id == player_id) { score -= 3; }
	
	double distance_to_enemy_planets = 0.0;
    for (hlt::Planet* p2 : enemy_planets) {
        distance_to_enemy_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_enemy_planets = distance_to_enemy_planets / std::max(1, int(enemy_planets.size()));

    double distance_to_owned_planets = 0.0;
    for (hlt::Planet* p2 : my_planets) {        
        distance_to_owned_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_owned_planets = distance_to_owned_planets / std::max(1, int(my_planets.size()));
	
	return avg_dist_to_owned_planets - avg_dist_to_enemy_planets;
	
}

double score_planet(hlt::Planet* p) {
    // LOWER scores are MORE valuable.

    double score;

    score = p->docking_spots * -1;
    if (p->owner_id == player_id) {
        score -= 2;     // We want to head towards planets we already own to speed up production.
    }
    if (p->docking_spots == 2 && my_planets.size() == 0) {
        score += 4;     // Penalty for size 2 planets in the beginning of the game. We want to use all 3 ships on a single planet if possible.
    }

    double distance_to_all_planets = 0.0;
    for (hlt::Planet* p2 : all_planets) {
        if (p == p2) { continue; }
        distance_to_all_planets += p->location.get_distance_to(p2->location);        
    }
    double avg_dist_to_all_planets = distance_to_all_planets / std::max(1, int(all_planets.size()));

    double distance_to_interesting_planets = 0.0;
    for (hlt::Planet* p2 : interesting_planets) {
        if (p == p2) { continue; }
        distance_to_interesting_planets += p->location.get_distance_to(p2->location);
        hlt::Log::log("Distance from " + to_string(p->entity_id) + " to " + to_string(p2->entity_id) + " is " + to_string(p->location.get_distance_to(p2->location)), 11);
    }
    double avg_dist_to_interesting_planets = distance_to_interesting_planets / std::max(1, int(interesting_planets.size()));

    double distance_to_enemy_planets = 0.0;
    for (hlt::Planet* p2 : enemy_planets) {
        distance_to_enemy_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_enemy_planets = distance_to_enemy_planets / std::max(1, int(enemy_planets.size()));

    double distance_to_owned_neutral_planets = 0.0;
    for (hlt::Planet* p2 : neutral_and_owned_planets) {
        if (p == p2) { continue; }
        distance_to_owned_neutral_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_owned_neutral_planets = distance_to_owned_neutral_planets / std::max(1, int(neutral_and_owned_planets.size()));

    double distance_to_my_planets = 0.0;
    for (hlt::Planet* p2 : my_planets) {
        if (p == p2) { continue; }
        distance_to_my_planets += p->location.get_distance_to(p2->location);
    }
    double avg_dist_to_my_planets = distance_to_my_planets / std::max(1, int(my_planets.size()));


    if (num_players == 2) {
        score += avg_dist_to_interesting_planets / hlt::constants::MAX_SPEED;
		if (p->owned && p->owner_id != player_id) {
			score += 1;
		}
    } else {
        score += 0.5 * avg_dist_to_owned_neutral_planets / hlt::constants::MAX_SPEED;
        //score += avg_dist_to_interesting_planets / hlt::constants::MAX_SPEED;

        if (enemy_planets.size() > 5) {
            score -= 1 * avg_dist_to_enemy_planets / hlt::constants::MAX_SPEED;
        }
        score -= (abs(p->location.pos_x - game_map->map_width / 2) / hlt::constants::MAX_SPEED) / 2;
        score -= (abs(p->location.pos_y - game_map->map_height / 3) / hlt::constants::MAX_SPEED) / 3;
        // score -= 0.25 * avg_dist_to_interesting_planets / hlt::constants::MAX_SPEED;
        /*
        if (!p->owned) {
            score -= 0.25 * avg_dist_to_enemy_planets / hlt::constants::MAX_SPEED;
        }
        */
        if (p->owned && p->owner_id != player_id) {
            // score += 10;
        }
    }


    hlt::Log::log("Planet " + to_string(p->entity_id) + " score: " + to_string(score), 10);
    return score;

}


void assign_p_ship_num() {
	for (hlt::Ship* s : all_ships) {
		std::unordered_map<int, int>::iterator i = ship_counter.find(s->entity_id);
		if (i == ship_counter.end()) {
			ship_counter[s->entity_id] = counter[s->owner_id];
			counter[s->owner_id]++;
		}
		s->p_ship_num = ship_counter[s->entity_id];
	}
}
